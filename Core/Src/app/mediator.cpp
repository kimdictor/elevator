#include "mediator.h"

extern "C" {
void getMediatorStatusTask(const void *mediator);
};
osThreadDef(getMediatorStatusTask, osPriorityNormal, 1, 0);

void getMediatorStatusTask(const void *mediator) {
  ((Mediator *)mediator)->statusTask();
}

void Mediator::init() {
  DebugPort::UART_Printf("init mainComs\r\n");
  main_coms.init();
  main_coms.setMediator(this);

  DebugPort::UART_Printf("init meditator status thread\r\n");
  if (osThreadCreate(osThread(getMediatorStatusTask), this) == NULL) {
    DebugPort::UART_Printf("fail to create status thread\r\n");
  }

  DebugPort::UART_Printf("init hardwares\r\n");
  for (int i = 0; i < ELEVATOR_DOOR_SYSTEM_COUNT; i++) {
    door_systems[i].init(DoorSystemHardwareDef[i].MotorUART,
                         DoorSystemHardwareDef[i].MotorDirPort,
                         DoorSystemHardwareDef[i].MotorDirPin,
                         DoorSystemHardwareDef[i].MotorPolarity,
                         DoorSystemHardwareDef[i].index);
    // internal_leds[i].init(DoorSystemHardwareDef[i].InternalLedPort,
    // DoorSystemHardwareDef[i].InternalLedPin);
    smart_films[i].init(DoorSystemHardwareDef[i].SmartFlimPort,
                        DoorSystemHardwareDef[i].SmartFlimPin);
    dc_fans[i].init(DoorSystemHardwareDef[i].DCFanPort,
                    DoorSystemHardwareDef[i].DCFanPin);

    door_systems[i].assignLed(&internal_leds[i]);
    door_systems[i].assignFlim(&smart_films[i]);
    door_systems[i].assignFan(&dc_fans[i]);
  }

  partition_sensors[0].init(PARTITION_SWITCH_1_GPIO_Port,
                            PARTITION_SWITCH_1_Pin);
  partition_sensors[1].init(PARTITION_SWITCH_2_GPIO_Port,
                            PARTITION_SWITCH_2_Pin);
  partition_sensors[2].init(PARTITION_SWITCH_3_GPIO_Port,
                            PARTITION_SWITCH_3_Pin);

  external_leds[0].init(&htim1, TIM_CHANNEL_1);
  external_leds[1].init(&htim1, TIM_CHANNEL_2);
  DebugPort::UART_Printf("init hardwares OK\r\n");

  DebugPort::UART_Printf("wait doors to ready\r\n");
  uint8_t okcnt = 0;
  while (true) {
    okcnt = 0;
    for (int i = 0; i < ELEVATOR_DOOR_SYSTEM_COUNT; i++) {
      if (door_systems[i].state != DOORSTATE_INIT) okcnt++;
    }
    if (okcnt == ELEVATOR_DOOR_SYSTEM_COUNT) break;
  }

  DebugPort::UART_Printf("mediator init OK\r\n");
}

void Mediator::processCommand(uint8_t index, uint8_t inst, uint8_t arg) {
  Blinky::Act.on();
  DebugPort::UART_Printf("processCommand: idx = %d, cmd = %d, arg = %d\r\n",
                         index, inst, arg);
  switch (inst) {
    case ELEVATOR_CMD_CONTROL_DOOR:
    case ELEVATOR_CMD_SET_INTERNAL_LED:
    case ELEVATOR_CMD_SET_SMART_FILM:
    case ELEVATOR_CMD_CONTROL_DC_FAN:
    case ELEVATOR_CMD_THRESHOLD_ADJUST: {
      if (index >= ELEVATOR_DOOR_SYSTEM_COUNT && index != 0x10) {
        DebugPort::UART_Printf("Illegal index for door system : %d\r\n", index);
        break;
      }
      door_systems[index].setCommandArg(arg);
      int32_t commandSignal = (DoorSystemSignals::kLast << (inst - 1));
      osSignalSet(door_systems[index].getThreadID(), commandSignal);
      break;
    }
    case ELEVATOR_CMD_SET_EXTERNAL_LED: {
      if (arg > 3) {
        DebugPort::UART_Printf("Illegal argument for external led : %d\r\n",
                               arg);
        break;
      }
      if (index != 0x10) {
        DebugPort::UART_Printf("Illegal index for external led : %d\r\n",
                               index);
        break;
      }
      external_leds[0].setMode(arg);
      external_leds[0].apply();
      external_leds[1].setMode(arg);
      external_leds[1].apply();
      break;
    }
  }
  Blinky::Act.off();
}

uint8_t Mediator::getStatusFlag() {
  uint8_t ret = 0;

  // bit 0 ~ 3 : external led #1, #2 mode
  ret |= external_leds[0].getMode();
  ret |= external_leds[1].getMode() << 2;

  // bit 4 ~ 6 : partition #1, #2, #3 status
  for (int i = 0; i < 3; i++) {
    ret |= (partition_sensors[i].read() ? 1 : 0) << 4 + i;
  }

  return ret;
}

void Mediator::statusTask() {
  uint8_t payload[8] = {
      0,
  };

  CAN_TxHeaderTypeDef txHeader;
  txHeader.ExtId = 0x201;
  txHeader.RTR = CAN_RTR_DATA;
  txHeader.IDE = CAN_ID_EXT;
  txHeader.DLC = 8;

  while (1) {
    Blinky::Run.on();
    for (int i = 0; i < ELEVATOR_DOOR_SYSTEM_COUNT; i++) {
      payload[i] = door_systems[i].getStatusFlag();
    }
    payload[4] = getStatusFlag();

    main_coms.Transmit(&txHeader, payload);
    osDelay(50);
    Blinky::Run.off();
    osDelay(50);
  }
}
#include "SerialReader.h"

ParsingState DynamixelSerialReader::getState(){
	return state;
}

bool DynamixelSerialReader::parsePacket(uint8_t recv_data){
	switch(state){
		case HEADERSTATE:
			if (header_count >= 3)
			{
					header_count = 0;
			}

			header[header_count++] = recv_data;

			if (header_count == 3)
			{
				if(header[0]==0xFF&&
					header[1]==0xFF&&
					header[2]==0xFD){
						infos.crc = 0;
						infos.recv_crc = 0;
						CRCCalculator::update_crc(&(infos.crc), header[0]);
						CRCCalculator::update_crc(&(infos.crc), header[1]);
						CRCCalculator::update_crc(&(infos.crc), header[2]);
						received_length = 0;
						state = RESERVEDSTATE;
				}
				else
				{
					for (int j = 0; j < 2; j++)
					{
							header[j] = header[j + 1];
					}
					header_count--;
				}
			}
			break;
		case RESERVEDSTATE:
			if (recv_data != 0xFD)
			{
				CRCCalculator::update_crc(&(infos.crc), recv_data);
        state = IDSTATE;
			}
			else
			{
        state = HEADERSTATE;
			}
			break;
		case IDSTATE:
			if (recv_data < 0xFD || recv_data == 0xFE)
			{
        infos.id = recv_data;
        CRCCalculator::update_crc(&(infos.crc), recv_data);
        state = LLSTATE;
			}
			else
			{
        state = HEADERSTATE;
			}
			break;
		case LLSTATE:
			infos.length = recv_data;
			CRCCalculator::update_crc(&(infos.crc), recv_data);
			state = LHSTATE;
			break;
		case LHSTATE:
			infos.length |= recv_data << 8;
			if (infos.length >= 3)
			{
					state = INSTSTATE;
					CRCCalculator::update_crc(&(infos.crc), recv_data);
			}
			else
			{
					state = HEADERSTATE;
			}
			break;
		case INSTSTATE:
			CRCCalculator::update_crc(&(infos.crc), recv_data);
			infos.inst = recv_data;

			if (recv_data == 0x55)
			{
					if (infos.length < 4 || infos.length > infos.param_buf_capacity + 4)
					{
							state = HEADERSTATE;
					}
					else
					{
							state = ERRORSTATE;
					}
			}
			else
			{
				state = HEADERSTATE;
			}
			break;
		case ERRORSTATE:
			CRCCalculator::update_crc(&(infos.crc), recv_data);

			if (infos.length == 4)
			{
        state = CRCLSTATE; //to CRC
			}
			else
			{
        state = PARAMSTATE; //to PARAM
			}
			break;
		case PARAMSTATE:
			received_buf[received_length++] = recv_data;
			CRCCalculator::update_crc(&(infos.crc), recv_data);
			
			if (infos.inst == 0x55)
			{
					if (received_length + 4 == infos.length)
					{
							infos.param = 0;
							for (int i = 0; i < (int)(infos.length - 4); i++)
							{
									infos.param |= received_buf[i] << 8 * i;
							}
							state = CRCLSTATE;
					}
			}
			else
			{
					if (received_length + 3 == infos.length)
					{
							infos.param = 0;
							for (int i = 0; i < (int)(infos.length - 4); i++)
							{
									infos.param |= received_buf[i] << 8 * i;
							}
							state = CRCLSTATE;
					}
			}
			break;
		case CRCLSTATE:
			infos.recv_crc = recv_data;
			state = CRCHSTATE;
			break;
		case CRCHSTATE:
			{
				infos.recv_crc |= recv_data << 8;
				if (infos.recv_crc == infos.crc)
				{
					state = HEADERSTATE;
					return true;
				}
				else
				{
					state = HEADERSTATE;
					return false;
				}
			}
			break;
	}
	return false;
}
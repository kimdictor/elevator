#include "blinky.h"

InverseBinaryGPIO Blinky::Dynamixel1Error(DXL_DEBUG_LED_1_GPIO_Port, DXL_DEBUG_LED_1_Pin);
InverseBinaryGPIO Blinky::Dynamixel2Error(DXL_DEBUG_LED_2_GPIO_Port, DXL_DEBUG_LED_2_Pin);
InverseBinaryGPIO Blinky::Dynamixel3Error(DXL_DEBUG_LED_3_GPIO_Port, DXL_DEBUG_LED_3_Pin);
InverseBinaryGPIO Blinky::Dynamixel4Error(DXL_DEBUG_LED_4_GPIO_Port, DXL_DEBUG_LED_4_Pin);
InverseBinaryGPIO Blinky::Run(LED_RUN_GPIO_Port, LED_RUN_Pin);
InverseBinaryGPIO Blinky::Act(LED_ACT_GPIO_Port, LED_ACT_Pin);
InverseBinaryGPIO Blinky::Err(LED_ERR_GPIO_Port, LED_ERR_Pin);

// simple delay function which doesn't depend on timer
void loopdelay(uint32_t tick) {
    uint64_t count = tick * 10000;
    while (1) {
        __ASM volatile ("NOP");
        count--;
        if (count <= 0) break;
    }
}

void Blinky::blink(InverseBinaryGPIO* io, uint8_t time) {
    while(1) {
        for (int i = 0; i < time; i++) {
            io->on();
            loopdelay(250);
            io->off();
            loopdelay(250);
        }
        loopdelay(1500);
    }
};

void Blinky::test() {
    for (int i = 0; i < BLINKY_COUNT; i++) { 
        Blinky::All[i]->on();
    }
}

void Blinky::reset() {
    for (int i = 0; i < BLINKY_COUNT; i++) { 
        Blinky::All[i]->off();
    }
}

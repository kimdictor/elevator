#include "main_coms.h"
#include "mediator.h"

extern "C" {
void mainComsTask(const void *arg);
};

MainComsController main_coms;

osThreadDef(mainComsTask,osPriorityNormal,1,0);
osMailQDef(CanRxQueue, 32, CanRxMessage);
osMailQDef(CanTxQueue, 32, CanTxMessage);

static inline int32_t CanMailbox2Index(uint32_t mailbox) {
  switch (mailbox) {
  case CAN_TX_MAILBOX0:
    return 0;
  case CAN_TX_MAILBOX1:
    return 1;
  case CAN_TX_MAILBOX2:
    return 2;
  default:
    return -1;
  }
}

/// Callbacks that handle mailbox transmission events
void HAL_CAN_TxMailbox0CompleteCallback(CAN_HandleTypeDef *hcan) {
  if (hcan == main_coms.get_can_port()) {
    main_coms.CompleteTx(CAN_TX_MAILBOX0);
  }
}

void HAL_CAN_TxMailbox1CompleteCallback(CAN_HandleTypeDef *hcan) {
  if (hcan == main_coms.get_can_port()) {
    main_coms.CompleteTx(CAN_TX_MAILBOX1);
  }
}

void HAL_CAN_TxMailbox2CompleteCallback(CAN_HandleTypeDef *hcan) {
  if (hcan == main_coms.get_can_port()) {
    main_coms.CompleteTx(CAN_TX_MAILBOX2);
  }
}

void HAL_CAN_TxMailbox0AbortCallback(CAN_HandleTypeDef *hcan) {
  if (hcan == main_coms.get_can_port()) {
    main_coms.AbortTx(CAN_TX_MAILBOX0);
  }
}

void HAL_CAN_TxMailbox1AbortCallback(CAN_HandleTypeDef *hcan) {
  if (hcan == main_coms.get_can_port()) {
    main_coms.AbortTx(CAN_TX_MAILBOX1);
  }
}

void HAL_CAN_TxMailbox2AbortCallback(CAN_HandleTypeDef *hcan) {
  if (hcan == main_coms.get_can_port()) {
    main_coms.AbortTx(CAN_TX_MAILBOX2);
  }
}

void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan) {
  if (hcan == main_coms.get_can_port()) {
    main_coms.Receive(CAN_RX_FIFO0);
  }
}

void HAL_CAN_RxFifo1MsgPendingCallback(CAN_HandleTypeDef *hcan) {
  if (hcan == main_coms.get_can_port()) {
    main_coms.Receive(CAN_RX_FIFO1);
  }
}

void HAL_CAN_ErrorCallback(CAN_HandleTypeDef *hcan) {
  if (hcan == main_coms.get_can_port()) {
    osSignalSet(main_coms.GetThreadId(), MainComsSignals::kError);
  }
}

/// Completes the CAN tranmission process by freeing the memory block of the
/// sent message
bool MainComsController::CompleteTx(uint32_t mailbox) {
  osStatus status;
  int32_t index = CanMailbox2Index(mailbox);
  if (index < 0) {
    DebugPort::UART_Printf("unknown mailbox index %d", mailbox);
    return false;
  }

  status = osMailFree(tx_queue, tx_msgs[index]);
  if (status != osOK) {
    DebugPort::UART_Printf("failed to free message block from mailbox %d (%x)", index,
                 status);
    return false;
  }

  return true;
}

/// Aborts an ongoing CAN transmission process and frees the memory block of the
/// message
bool MainComsController::AbortTx(uint32_t mailbox) {
  osStatus status;
  int32_t index = CanMailbox2Index(mailbox);
  if (index < 0) {
    DebugPort::UART_Printf("unknown mailbox index %d", mailbox);
    return false;
  }

  status = osMailFree(tx_queue, tx_msgs[index]);
  if (status != osOK) {
    DebugPort::UART_Printf("failed to free message block from mailbox %d (%x)", index,
                 status);
    return false;
  }

  return true;
}

/// Transmit can message.
bool MainComsController::Transmit(CAN_TxHeaderTypeDef *_header, uint8_t *_data) {
  osStatus status;
  HAL_StatusTypeDef hal_status;

  CanTxMessage *message = (CanTxMessage *)osMailAlloc(tx_queue, 0);
  if (message == NULL) {
    DebugPort::UART_Printf("failed to allocate CAN Tx message");
    return false;
  }

  memcpy(&message->header, _header, sizeof(struct _CanTxMessage));
  memcpy(message->data, _data, _header->DLC);

  status = osMailPut(tx_queue, message);
  if (status != osOK) {
    DebugPort::UART_Printf("failed to put message in transmit queue (%d)", status);
    osMailFree(tx_queue, message);
    return false;
  }

  osSignalSet(threadID, MainComsSignals::kTransmit);
  return true;
}

/// Receives CAN messages from a FIFO. Gets the message from the FIFO and
/// enqueues the message data and sets a signal to wake up the task for
/// handling
bool MainComsController::Receive(uint32_t fifo) {
  osStatus status;
  HAL_StatusTypeDef hal_status;
  CanRxMessage *message = (CanRxMessage *)osMailAlloc(rx_queue, 0);
  if (message == NULL) {
    DebugPort::UART_Printf("failed to allocate CAN Rx message");
    return false;
  }

  hal_status =
      HAL_CAN_GetRxMessage(can_port, fifo, &message->header, message->data);
  if (hal_status != HAL_OK) {
    DebugPort::UART_Printf("failed to get CAN Rx message (%x)",
                 hal_status);
    goto error;
  }

  status = osMailPut(rx_queue, message);
  if (status != osOK) {
    DebugPort::UART_Printf("failed to put message in receive queue (%d)", status);
    goto error;
  }

  osSignalSet(threadID, MainComsSignals::kReceive);
  return true;

error:
  osMailFree(rx_queue, message);
  return false;
}

/// Handles messages received over the CAN network and sends a packet over the
/// network
bool MainComsController::ProcessRxMessages(void) {
  int i;
  osStatus status;
  CanRxMessage *msg;
  osEvent event;

  while (true) {
    event = osMailGet(rx_queue, 0); //!< Try logic
    if (event.status == osEventMail) {
      msg = (CanRxMessage *)event.value.p;
			
			bool msg_ret = true;
			
			if((msg->header.ExtId&0x0100)!=0x100)
			{
				msg_ret = false;
				DebugPort::UART_Printf("CAN_ID from baseboard is not 0x0100 (rx can id:%X)",msg->header.ExtId);
			}
			
			if((msg->header.DLC != 4)){
				msg_ret = false;
				DebugPort::UART_Printf("DLC from baseboard is not 3 (DLC:%X)",msg->header.DLC);
			}
			
			if(msg_ret){
				//msg->data[0] : door_index
				//msg->data[1] : inst
				//msg->data[2] : arg

        CAN_TxHeaderTypeDef txHeader;
        txHeader.ExtId = 0x200;
        txHeader.RTR = CAN_RTR_DATA;
        txHeader.IDE = CAN_ID_EXT;
        txHeader.DLC = 3;
        uint8_t payload[3] = {msg->data[0], msg->data[1], msg->data[2]};
        MainComsController::Transmit(&txHeader, payload);
        DebugPort::UART_Printf("CAN return packet sent\r\n");
				mediator->processCommand(msg->data[0],msg->data[1],msg->data[2]);
			}

      // Free after sending
      status = osMailFree(rx_queue, event.value.p);
      if (status != osOK) {
        DebugPort::UART_Printf("failure to free mail slot (%x)",status);
        return false;
      }
    }
    else if (event.status != osOK) {
      DebugPort::UART_Printf("failure waiting for receive message queue (%x)", event.status);
      return false;
    } else {
      break; // No messages left in the queue
    }
  }
  return true;
}

/// Handles messages that have been queued for transmission over the CAN bus
/// @TODO This function needs to be made thread-safe with respect to the CAN
/// interrupts that may be called after adding the Tx message
///
/// @TODO Implement some pacing and locking mechanisms to handle packets sent
/// over the network that may occur more frequently than the CAN is capable of
/// transmitting
bool MainComsController::ProcessTxMessages(void) {
  int32_t index;
  HAL_StatusTypeDef status;
  osEvent event;
  CanTxMessage *message;

  while (true) {
    event = osMailGet(tx_queue, 0);
    if (event.status != osEventMail) {
      break;
    }

    message = (CanTxMessage *)event.value.p;
    status = HAL_CAN_AddTxMessage(can_port, &message->header, message->data,
                                  &message->mailbox);
    if (status != HAL_OK) {
      DebugPort::UART_Printf("failed to put message in Tx mailbox (%d)", status);
      return false;
    }

    index = CanMailbox2Index(message->mailbox);
    if (index < 0) {
      DebugPort::UART_Printf("unknown mailbox index %d",message->mailbox);
      return false;
    }

    // Store a pointer to the message so it can be freed when transmission
    // complete
    tx_msgs[index] = message;
  }

  return true;
}

bool MainComsController::HandleError(void) {
  HAL_CAN_Stop(can_port);
  DebugPort::UART_Printf("CAN relay error (%x, ESR=%x)\r\n", HAL_CAN_GetError(can_port), can_port->Instance->ESR);
  HAL_CAN_AbortTxRequest(can_port,
                         CAN_TX_MAILBOX0 | CAN_TX_MAILBOX1 | CAN_TX_MAILBOX2);
  HAL_CAN_ResetError(can_port);
  HAL_CAN_Start(can_port);
  Blinky::Err.toggle();
  return true;
}


void mainComsTask(const void *arg){
	((MainComsController*)arg)->task();
}

bool MainComsController::init(){
 
	init_can();
	
	threadID = osThreadCreate(osThread(mainComsTask),this);
	if(threadID==NULL){
		DebugPort::UART_Printf("failed to create main coms thread\r\n");
		return false;
	}
  tx_queue = osMailCreate(osMailQ(CanTxQueue), NULL);
  if (tx_queue == NULL) {
    DebugPort::UART_Printf("failed to initiate tx queue\r\n");
    return false;
  }
  rx_queue = osMailCreate(osMailQ(CanRxQueue), NULL);
  if (rx_queue == NULL) {
    DebugPort::UART_Printf("failed to initiate rx queue\r\n");
    return false;
  }
}

CAN_HandleTypeDef * MainComsController::get_can_port()
{
	return can_port;
}

bool MainComsController::init_can()
{
	//init CAN
	can_port = &CAN_PORT;
	HAL_StatusTypeDef status;
  CAN_FilterTypeDef filter;

  // Not filtering any messages
  filter.FilterIdHigh = 0x0000;
  filter.FilterIdLow = 0x0000;
  filter.FilterMaskIdHigh = 0x0000;
  filter.FilterMaskIdLow = 0x0000;
  filter.FilterFIFOAssignment = CAN_FILTER_FIFO0;
  filter.FilterBank = 0;
  filter.FilterMode = CAN_FILTERMODE_IDMASK;
  filter.FilterActivation = ENABLE;
  filter.FilterScale = CAN_FILTERSCALE_32BIT;
  status = HAL_CAN_ConfigFilter(can_port, &filter);
  
	if (status != HAL_OK) {
		DebugPort::UART_Printf("failed to configure filter for FIFO0 (%x)", status);
    return false;
  }
	
  filter.FilterFIFOAssignment = CAN_FILTER_FIFO1;
  status = HAL_CAN_ConfigFilter(can_port, &filter);
  if (status != HAL_OK) {
    DebugPort::UART_Printf("failed to configure filter for FIFO1 (%x)", status);
    return false;
  }

  status = HAL_CAN_ActivateNotification(
      can_port, CAN_IT_TX_MAILBOX_EMPTY | CAN_IT_RX_FIFO0_MSG_PENDING |
                 CAN_IT_RX_FIFO0_FULL | CAN_IT_RX_FIFO0_OVERRUN |
                 CAN_IT_RX_FIFO1_MSG_PENDING | CAN_IT_RX_FIFO1_FULL |
                 CAN_IT_RX_FIFO1_OVERRUN | CAN_IT_ERROR);
  if (status != HAL_OK) {
    DebugPort::UART_Printf("failed to configure CAN notifications (%d)\r\n", status);
    return false;
  }

  status = HAL_CAN_Start(can_port);
  if (status != HAL_OK) {
    DebugPort::UART_Printf("failed to start CAN (%08x != %08x)\r\n", status, HAL_OK);
    DebugPort::UART_Printf("state=%d, err=%d\r\n", hcan1.State, hcan1.ErrorCode);
    return false;
  }
}

void MainComsController::setMediator(Mediator *_mediator){
	mediator = _mediator;
}

void MainComsController::task(){
	osEvent event;
	
	while(1){
		event = osSignalWait(0,osWaitForever);
		if(event.status == osEventSignal){
			if (event.value.signals & MainComsSignals::kReceive) {
        ProcessRxMessages();
      }

      if (event.value.signals & MainComsSignals::kTransmit) {
        ProcessTxMessages();
      }

      if (event.value.signals & MainComsSignals::kError) {
        HandleError();
      }
		}
	}
}

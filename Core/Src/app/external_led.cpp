#include "external_led.h"

// Init neopixel, start timer dma
void ExternalLED::init(TIM_HandleTypeDef * handle, uint8_t channel){
	timHandle = handle;
	timChannel = channel;
	HAL_TIM_PWM_Start_DMA(timHandle, timChannel, (uint32_t *)dmaBuffer, NEOPIXEL_DMADATA_LENGTH);
}

// set color to given number's led. RGB is 8-bits grayscale and number starts from zero.
void ExternalLED::setLED(int number, uint8_t R, uint8_t G, uint8_t B) {
	data[number * NEOPIXEL_COLOR_PER_LED] = G;
	data[number * NEOPIXEL_COLOR_PER_LED + 1] = R;
	data[number * NEOPIXEL_COLOR_PER_LED + 2] = B;
}

// apply set color value to dma buffer. This is kind of double-buffering.
void ExternalLED::apply(){
	for (int led = 0; led < NEOPIXEL_LED; led++) {
		for (int bit = 0; bit < NEOPIXEL_PACKET_SIZE_PER_COLOR; bit++) {
			dmaBuffer[(led * NEOPIXEL_PACKET_SIZE_PER_LED) + (NEOPIXEL_PACKET_SIZE_PER_COLOR * 0) + bit] = data[(led * NEOPIXEL_COLOR_PER_LED)] & (1 << bit) > 0 ? NEOPIXEL_HIGH_PULSE_COUNT : NEOPIXEL_LOW_PULSE_COUNT;
			dmaBuffer[(led * NEOPIXEL_PACKET_SIZE_PER_LED) + (NEOPIXEL_PACKET_SIZE_PER_COLOR * 1) + bit] = data[(led * NEOPIXEL_COLOR_PER_LED) + 1] & (1 << bit) > 0 ? NEOPIXEL_HIGH_PULSE_COUNT : NEOPIXEL_LOW_PULSE_COUNT;
			dmaBuffer[(led * NEOPIXEL_PACKET_SIZE_PER_LED) + (NEOPIXEL_PACKET_SIZE_PER_COLOR * 2) + bit] = data[(led * NEOPIXEL_COLOR_PER_LED) + 2] & (1 << bit) > 0 ? NEOPIXEL_HIGH_PULSE_COUNT : NEOPIXEL_LOW_PULSE_COUNT;
		}
	}
}

// set color of whole leds. RGB data type is equal to setLED()'s 
void ExternalLED::setLEDAll(uint8_t R, uint8_t G, uint8_t B) {
	for (int l = 0; l < NEOPIXEL_LED; l++) {
		setLED(l, R, G, B);
	}
}

// set mode of external led. this would cause change whole led's color.
// mode 0 = off, mode 1 = on, mode 2 = red, mode 3 = blue
void ExternalLED::setMode(uint8_t mode) {
	if (mode >= EXTERNAL_LED_MODE_COUNT) return;
	this->mode = mode;
	setLEDAll(modeRGB[mode][0], modeRGB[mode][1], modeRGB[mode][2]);
}

uint8_t ExternalLED::getMode() {
	return mode;
}

// stop timer dma.
void ExternalLED::stop() {
	HAL_TIM_PWM_Stop_DMA(timHandle, timChannel);
}

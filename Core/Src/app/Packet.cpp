#include "Packet.h"

DynamixelPacketMaker::DynamixelPacketMaker()
{
    packet.buf_length = 0;
}

void DynamixelPacketMaker::makePacket()
{
    packet.buf_length = 0;
    //make header
    packet.packet_buf[packet.buf_length++] = 0xFF;
    packet.packet_buf[packet.buf_length++] = 0xFF;
    packet.packet_buf[packet.buf_length++] = 0xFD;
    packet.packet_buf[packet.buf_length++] = 0x00;
    //make ID
    packet.packet_buf[packet.buf_length++] = infos.ID;

    switch (infos.inst)
    {
			case INSTRUCTIONTYPE_PING:
			case INSTRUCTIONTYPE_REBOOT:
					packet.packet_buf[packet.buf_length++]=0x03;
					packet.packet_buf[packet.buf_length++]=0x00;
					packet.packet_buf[packet.buf_length++]=infos.inst;
					break;
			case INSTRUCTIONTYPE_FACTORYRESET:
					packet.packet_buf[packet.buf_length++]=0x04;
					packet.packet_buf[packet.buf_length++]=0x00;
					packet.packet_buf[packet.buf_length++]=infos.inst;
					packet.packet_buf[packet.buf_length++]=0x01;
					break;
			case INSTRUCTIONTYPE_WRITE:
			case INSTRUCTIONTYPE_READ:
					//make Length
					packet.packet_buf[packet.buf_length++] = (infos.paramLength + 5) >> 0;
					packet.packet_buf[packet.buf_length++] = (infos.paramLength + 5) >> 8;

					//make instruction
					packet.packet_buf[packet.buf_length++] = infos.inst;

					//make address
					packet.packet_buf[packet.buf_length++] = infos.address >> 0;
					packet.packet_buf[packet.buf_length++] = infos.address >> 8;

					//make parameter
					for (int i = 0; i < infos.paramLength; i++)
					{
							packet.packet_buf[packet.buf_length++] = infos.param[i];
					}
					break;
			default:
					break;
			}
    uint16_t crcNumber = 0;
    for(uint8_t i=0;i<packet.buf_length;i++){
        CRCCalculator::update_crc(&crcNumber,packet.packet_buf[i]);
    }
    packet.packet_buf[packet.buf_length++] = crcNumber >> 0;
    packet.packet_buf[packet.buf_length++] = crcNumber >> 8;
}
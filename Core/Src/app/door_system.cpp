#include "door_system.h"

#include "mediator.h"

extern "C" {
void doorSystemTask(const void* arg);
};

osThreadDef(doorSystemTask, osPriorityNormal, 4, 0);

void doorSystemTask(const void* arg) {
    ((DoorSystem*)arg)->task();
}

DoorSystem::DoorSystem() {
    loopTick = 0;
}

/*
LEFT : OPEN = HIGH, CLOSE = LOW
RIGHT : OPEN = LOW, CLOSE = HIGH
*/
bool DoorSystem::init(
    UART_HandleTypeDef* dxl_serial_port,
    GPIO_TypeDef* dir_port,
    uint16_t dir_pin,
    DoorPolarity polarity,
    uint8_t errorLedIndex) {
    this->polarity = polarity;
    this->errorLedIndex = errorLedIndex;
    switch (polarity) {
        case DOORPOLARITY_LEFT:
            lowPulse = OPENED_PULSE_LEFT;
            highPulse = CLOSED_PULSE_LEFT;
            break;
        case DOORPOLARITY_RIGHT:
            lowPulse = OPENED_PULSE_RIGHT;
            highPulse = CLOSED_PULSE_RIGHT;
            break;
    }
		
        /*
    if (thresholdStore::hasRecord(errorLedIndex)) {
        thresholdStore::getRecord(errorLedIndex, &lowPulse, &highPulse);
        log("apply threshold record!");
    }
*/
    motor.init(dxl_serial_port, dir_port, dir_pin);
    threadID = osThreadCreate(osThread(doorSystemTask), this);
    if (threadID == NULL) {
        error(DOORSYSTEMERROR_THREAD_INIT_FAILED);
        return false;
    }
}

void DoorSystem::setCommandArg(uint8_t _command_arg) {
    this->command_arg = _command_arg;
}

osThreadId DoorSystem::getThreadID() {
    return threadID;
}

void DoorSystem::setMediator(Mediator* _mediator) {
    mediator = _mediator;
}

void DoorSystem::assignLed(InternalLED* _led) {
    led = _led;
}

void DoorSystem::assignLed(GPIO_TypeDef* _hotport, uint16_t _hotpin, GPIO_TypeDef* _coldport, uint16_t _coldpin) {
    led = new InternalLED();
    led->init(_hotport, _hotpin, _coldport, _coldpin);
}

void DoorSystem::assignFan(DCFan* _fan) {
    fan = _fan;
}
void DoorSystem::assignFan(GPIO_TypeDef* _port, uint16_t _pin) {
    fan = new DCFan();
    fan->init(_port, _pin);
}

void DoorSystem::assignFlim(SmartFlim* _flim) {
    flim = _flim;
}

void DoorSystem::assignFlim(GPIO_TypeDef* _port, uint16_t _pin) {
    flim = new SmartFlim();
    flim->init(_port, _pin);
}

void DoorSystem::openDoor() {
    if (state != DOORSTATE_CLOSED && state != DOORSTATE_CLOSING) return;

    while (true) {
        motor.goalPosition(polarity == DOORPOLARITY_LEFT ? highPulse : lowPulse);
        if (motor.getComsResult() == MOTOR_COMS_RESULT_OK) {
            break;
        }
    }
    commandTime = HAL_GetTick();
    pwmErrorCount = 0;
    pwmPassContinuousCount = 0;
}

void DoorSystem::closeDoor() {
    if (state != DOORSTATE_OPENED && state != DOORSTATE_OPENING) return;

    while (true) {
        motor.goalPosition(polarity == DOORPOLARITY_LEFT ? lowPulse : highPulse);
        if (motor.getComsResult() == MOTOR_COMS_RESULT_OK) {
            break;
        }
    }
    commandTime = HAL_GetTick();
    pwmErrorCount = 0;
    pwmPassContinuousCount = 0;
}

void DoorSystem::ThresholdAdjust() {
    int high = 0, low = 9999, current = motor.getPresentPosition(), pos = 0;
    bool isIncrease = false;

    while (true) {
        //log("adjust pos = %d, current = %d", current, motor.getPresentCurrent;
        motor.goalPosition(current);
        if (motor.getComsResult() == MOTOR_COMS_RESULT_OK) {
            osDelay(500);
            pos = motor.getPresentPosition();
            if (abs(pos - current) < 60) {
            EXIT_FAIL:
                current += isIncrease ? 20 : -20;
            } else {
                for (int i = 0; i < 5; i++) {
                    osDelay(500);
                    pos = motor.getPresentPosition();
                    if (abs(pos - current) < 60) {
                        goto EXIT_FAIL;
                    }
                }
                log("adjust reached threshold, real = %d, expected = %d", pos, current);
                if (isIncrease) {
                    high = pos;
                    break;
                } else {
                    low = pos;
                    current = pos;
                    isIncrease = true;
                }
            }
        }
    }
    log("adjust complete, max=%d, min=%d", high, low);
    lowPulse = low;
    highPulse = high;
    initEEPROM();
    /*
    if (!thresholdStore::setRecord(errorLedIndex, low, high)) {
        log("fail to save threshold value");
    }
    */
    return;
}

void DoorSystem::repeatTest() {
    openDoor();

    uint32_t t1 = HAL_GetTick();
    while (HAL_GetTick() < t1 + 3000) {
        int curPWM = motor.getPresentPWM();
        int curPosition = motor.getPresentPosition();
        osDelay(10);
    }
    closeDoor();

    t1 = HAL_GetTick();
    while (HAL_GetTick() < t1 + 3000) {
        int curPosition = motor.getPresentPosition();
        int curPWM = motor.getPresentPWM();
        osDelay(10);
    }
}

bool DoorSystem::initEEPROM() {
    motor.operationMode(3);
    motor.setDriveMode(false, true, false);
    motor.setMaxPositionLimit(highPulse + 50);
    motor.setMinPositionLimit(lowPulse - 50);
    return true;
}

bool DoorSystem::initRAM() {
    motor.setProfileAcceleration(ACCEL_TIME);
    motor.setProfileVelocity(TOTAL_TIME);
    return true;
}

void DoorSystem::initDoor() {
}

void DoorSystem::run() {
    DoorState originalState = state;
    switch (state) {
        case DOORSTATE_INIT: {
            // 1-1) check connection
            if (!motor.ping()) {
                // 1-2) setBaudRate if ping failed
                if (!setBaudrate()) {
                    error(DOORSYSTEMERROR_MOTOR_DISCONNECTED);
                    break;
                }
            }
            log("ping success");

            // 2) check model number
            if (!motor.isModelNumberOkay) {
                error(DOORSYSTEMERROR_MOTOR_MODEL_ERROR);
                break;
            }

            // 3) init EEPROM
            initEEPROM();

            // 4) init RAM
            initRAM();

            // 5) torque On
            motor.torqueOn();

            // 6) init door
            int currentPosition = motor.getPresentPosition();
            if (abs(currentPosition - highPulse) < CLOSE_THRESHOLD) {
                state = polarity == DOORPOLARITY_LEFT ? DOORSTATE_OPENED : DOORSTATE_CLOSED;
            } else if (abs(currentPosition - lowPulse) < OPEN_THRESHOLD){
                state = polarity == DOORPOLARITY_LEFT ? DOORSTATE_CLOSED : DOORSTATE_OPENED;
            } else {
                state = DOORSTATE_OPENED;
                closeDoor();
            }
        } break;
        case DOORSTATE_OPENED:
        case DOORSTATE_CLOSED:
            break;
        case DOORSTATE_OPENING:
        case DOORSTATE_CLOSING: {
            loopTick = HAL_GetTick();
            if (HAL_GetTick() - commandTime < DELAY_GOALPOSITION) {
                // wait opening or closing
                break;
            }

            int curPosition = motor.getPresentPosition();
            if (motor.getComsResult() == MotorComsResult::MOTOR_COMS_RESULT_READTIMEOUT) {
                error(DOORSYSTEMERROR_MOTOR_DISCONNECTED);
                break;
            }
            // # check mission complete
            // 1. get currentPosition
            // int curPosition = motor.getPresentPosition();
            // 2. check currentPosition is OpeningEdge
            if (state == DOORSTATE_OPENING) {
                if (abs(curPosition - (polarity == DOORPOLARITY_LEFT ? highPulse : lowPulse)) < OPEN_THRESHOLD) {
                    state = DOORSTATE_OPENED;
                }
            } else {
                if (abs(curPosition - (polarity == DOORPOLARITY_LEFT ? lowPulse : highPulse)) < CLOSE_THRESHOLD) {
                    state = DOORSTATE_CLOSED;
                }
            }

            int positionTrajectory = motor.getPositionTrajectory();
            if (motor.getComsResult() == MotorComsResult::MOTOR_COMS_RESULT_READTIMEOUT) {
                error(DOORSYSTEMERROR_MOTOR_COMM_FAILED);
                break;
            }

            if (abs(positionTrajectory - curPosition) > 70) {
                log("recovery activate: current=%d, target=%d", curPosition, positionTrajectory);
                float ratio = ((float)(curPosition - lowPulse)) / (highPulse - lowPulse);
                uint16_t temp_total_time = TOTAL_TIME * ratio;
                uint16_t temp_accel_time = ACCEL_TIME * ratio;
                motor.setProfileVelocity(temp_total_time);
                motor.setProfileAcceleration(temp_accel_time);

                if (state == DOORSTATE_OPENING) {
                    closeDoor();
                    state = DOORSTATE_RECOVERYCLOSING;
                } else {
                    openDoor();
                    state = DOORSTATE_RECOVERYOPENING;
                }
            }

            // DebugPort::UART_Printf("loopInterval :: %u\r\n",HAL_GetTick()-loopTick);
            // DebugPort::UART_Printf("%d//%d//%d\r\n",positionTrajectory,curPosition,positionTrajectory-curPosition);
            break;
        }
        case DOORSTATE_RECOVERYOPENING:
        case DOORSTATE_RECOVERYCLOSING: {
            // # check mission complete
            // 1. get currentPosition
            int curPosition = motor.getPresentPosition();
            if (motor.getComsResult() == MotorComsResult::MOTOR_COMS_RESULT_READTIMEOUT) {
                error(DOORSYSTEMERROR_MOTOR_COMM_FAILED);
                break;
            }
            // 2. check currentPosition is OpeningEdge

            motor.setProfileVelocity(TOTAL_TIME);
            motor.setProfileAcceleration(ACCEL_TIME);
            if (state == DOORSTATE_RECOVERYOPENING) {
                if (abs(curPosition - (polarity == DOORPOLARITY_LEFT ? highPulse : lowPulse)) < OPEN_THRESHOLD) {
                    state = DOORSTATE_OPENED;
                }
            } else {
                if (abs(curPosition - (polarity == DOORPOLARITY_LEFT ? lowPulse : highPulse)) < CLOSE_THRESHOLD) {
                    state = DOORSTATE_CLOSED;
                }
            }
            break;
        }
        case DOORSTATE_REBOOT:
            break;
    }

    if (state != DOORSTATE_INIT) {
        if (motor.getHardwareErrorStatus() > 0) {
            error(DOORSYSTEMERROR_MOTOR_HARDWARE_ERROR);
            motor.reboot();
            state = DOORSTATE_INIT;
        }
    }

    if (originalState != state) {
        log("state changed : %s", stateToString(state));
    }
    osDelay(5);
}

// get status byte for transmitting to pc
uint8_t DoorSystem::getStatusFlag() {
    uint8_t ret = 0;

    // bit 0 ~ 2 : door status
    switch (state) {
        case DOORSTATE_OPENED:
            ret |= 0b00000001;
            break;
        case DOORSTATE_CLOSED:
            ret |= 0b00000000;
            break;
        case DOORSTATE_OPENING:
            ret |= 0b00000011;
            break;
        case DOORSTATE_CLOSING:
            ret |= 0b00000010;
            break;
        case DOORSTATE_RECOVERYCLOSING:
            ret |= 0b00000110;
            break;
        case DOORSTATE_RECOVERYOPENING:
            ret |= 0b00000111;
            break;
            // default:
            //  there is no exceptional door status flag on specification
    }

    // bit 3 ~ 4 : internal led status

    // bit 5 : torque enable
    /* HARDFAULT caused by here
    if (motor.getTorqueEnable()) {
            ret |= 0b00100000;
    }
    */

    // bit 6 : smart film status
    if (flim->read()) {
        ret |= 0b01000000;
    }

    // bit 7 : dc fan status
    if (fan->read()) {
        ret |= 0b10000000;
    }

    return ret;
}

// set baudrate to BAUDRATE
bool DoorSystem::setBaudrate() {
#define BAUDRATE_COUNT 3
    static uint32_t baudrate[BAUDRATE_COUNT] = {57600, 115200, 1000000};

    for (int i = 0; i < BAUDRATE_COUNT; i++) {
        motor.setPortBaudRate(baudrate[i]);
        if (motor.ping()) {
            log("baudrate found = %d", baudrate[i]);
            return true;
        }
    }
    log("baudrate found fail");
    return false;
}

void DoorSystem::commandToMotor(void (DynamixelProtocolController::*f)(int), int param) {
    (motor.*f)(param);
}

void DoorSystem::error(DoorSystemError err) {
    log("error: %s", errorToString(err));
    Blinky::DynamixelError[errorLedIndex]->toggle();
}

void DoorSystem::log(char* format, ...) {
#define PRINT_BUFFER_LENGTH 128
    char print_buffer[PRINT_BUFFER_LENGTH];
    va_list arg;
    uint16_t len;
    va_start(arg, format);
    vsnprintf(print_buffer, PRINT_BUFFER_LENGTH, format, arg);
    va_end(arg);
    DebugPort::UART_Printf("DoorSystem[%d]: %s\r\n", errorLedIndex, print_buffer);
}

void DoorSystem::task() {
    state = DOORSTATE_INIT;

    osEvent event;
    while (1) {
        event = osSignalWait(0, LOOP_INTERVAL);
        if (event.status == osEventSignal) {
            if (event.value.signals & DoorSystemSignals::DOOR_SYSTEM_SIGNAL_CONTROL_DOOR) {
                switch (command_arg) {
                    case DoorCommandArgument::CLOSE: {
                        log("close door");
                        closeDoor();
                        state = DOORSTATE_CLOSING;
                        break;
                    }
                    case DoorCommandArgument::OPEN: {
                        log("open door");
                        openDoor();
                        state = DOORSTATE_OPENING;
                        break;
                    }
                    default: {
                        log("unknown door argument: %x", command_arg);
                    }
                }
            }

            if (event.value.signals & DoorSystemSignals::DOOR_SYSTEM_SIGNAL_SET_INTERNAL_LED) {
                led->setMode(command_arg);
                log("INTERNAL LED Command with arg(%x)", command_arg);
            }

            if (event.value.signals & DoorSystemSignals::DOOR_SYSTEM_SIGNAL_SET_SMART_FILM) {
                switch (command_arg) {
                    case DoorCommandArgument::ON: {
                        flim->on();
                        break;
                    }
                    case DoorCommandArgument::OFF: {
                        flim->off();
                        break;
                    }
                    default: {
                    }
                }
                log("SMART FILM Command with arg(%x)", command_arg);
            }

            if (event.value.signals & DoorSystemSignals::DOOR_SYSTEM_SIGNAL_SET_DC_FAN) {
                switch (command_arg) {
                    case DoorCommandArgument::ON: {
                        fan->on();
                        break;
                    }
                    case DoorCommandArgument::OFF: {
                        fan->off();
                        break;
                    }
                    default:
                        break;
                }
                log("DC FAN Command with arg(%x)", command_arg);
            }

            if (event.value.signals & DoorSystemSignals::DOOR_SYSTEM_SIGNAL_THRESHOLD_ADJUST) {
                log("threshold adjust");
                ThresholdAdjust();
            }
        }
        run();
    }
}

const char* DoorSystem::errorToString(DoorSystemError err) {
    switch (err) {
        case DoorSystemError::DOORSYSTEMERROR_MOTOR_DISCONNECTED:
            return "MOTOR_DISCONNECTED";
        case DoorSystemError::DOORSYSTEMERROR_MOTOR_MODEL_ERROR:
            return "MOTOR_MODEL_ERROR";
        case DoorSystemError::DOORSYSTEMERROR_MOTOR_PING_FAILED:
            return "MOTOR_PING_FAILED";
        case DoorSystemError::DOORSYSTEMERROR_MOTOR_SET_BAUD_FAILED:
            return "MOTOR_SET_BAUT_FAILED";
        case DoorSystemError::DOORSYSTEMERROR_MOTOR_HARDWARE_ERROR:
            return "MOTOR_HARDWARE_ERROR";
        case DoorSystemError::DOORSYSTEMERROR_MOTOR_COMM_FAILED:
            return "MOTOR_HARDWARE_ERROR";
        case DoorSystemError::DOORSYSTEMERROR_THREAD_INIT_FAILED:
            return "THREAD_INIT_FAILED";
        case DoorSystemError::DOORSYSTEMERROR_NONE:
            return "NONE";
        default:
            return "UNKNOWN";
    }
}

const char* DoorSystem::stateToString(DoorState state) {
    switch (state) {
        case DOORSTATE_INIT:
            return "INIT";
        case DOORSTATE_OPENED:
            return "OPENED";
        case DOORSTATE_CLOSED:
            return "CLOSED";
        case DOORSTATE_OPENING:
            return "OPENING";
        case DOORSTATE_CLOSING:
            return "CLOSING";
        case DOORSTATE_RECOVERYOPENING:
            return "RECOVERYOPENING";
        case DOORSTATE_RECOVERYCLOSING:
            return "RECOVERYCLOSING";
        case DOORSTATE_REBOOT:
            return "REBOOT";
    }
}
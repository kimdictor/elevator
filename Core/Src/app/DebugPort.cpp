#include "DebugPort.h"

UART_HandleTypeDef *DebugPort::huart = NULL;
uint8_t* DebugPort::print_buffer = new uint8_t[128];

void DebugPort::UART_Printf(char *format, ...)
{
		va_list arg;
		uint16_t len;

		va_start(arg, format);

		len = vsnprintf((char *)print_buffer, 128, format, arg);

		HAL_UART_Transmit(huart, print_buffer, len, 0xFFFF);

		va_end(arg);
}
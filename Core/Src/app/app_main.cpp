#include "app_main.h"
#include <stdint.h>
#include <stdio.h>

//cpp headers
#include "mediator.h"
#include "DebugPort.h"
#include "device_config.h"

#ifdef __cplusplus
extern "C" {
#endif
void mainComsThreadTask(const void *arg);
#ifdef __cplusplus
}
#endif


// MainComsController main_coms;
Mediator mediator;

void app_main(void){
	blinkyTest();
	osDelay(500);
	blinkyReset();

	DebugPort::setPort(&DEBUG_SERIAL);

	DebugPort::UART_Printf("init mediator\r\n");
	mediator.init();
	DebugPort::UART_Printf("init finished\r\n");

	while(1){
		osThreadYield();         
	}
}
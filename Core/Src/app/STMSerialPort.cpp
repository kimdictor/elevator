#include "STMSerialPort.h"
#include "DebugPort.h"

STM32SerialPort::STM32SerialPort()
{
}

void STM32SerialPort::init(UART_HandleTypeDef *_huart){
	huart = _huart;
	
	HAL_UART_Receive_DMA(huart, queue, MAXBUFFERSIZE);
	CLEAR_BIT(huart->Instance->CR1,USART_CR1_PEIE);
	CLEAR_BIT(huart->Instance->CR3,USART_CR3_EIE);

	front = 0;
	rear = 0;
}

void STM32SerialPort::checkError() {
	if (huart->Instance->SR | 0x00001011 > 0) { // check ORE, PE, FE register is set
		HAL_UART_DeInit(huart);
		HAL_UART_Init(huart);
		HAL_UART_Receive_DMA(huart, queue, MAXBUFFERSIZE);
	}
}

void STM32SerialPort::setBaudrate(uint32_t baudrate)
{
	huart->Init.BaudRate = baudrate;
	HAL_UART_Init(huart);
}

void STM32SerialPort::reset(){
	rear = front;
}

void STM32SerialPort::write(uint8_t data){
	HAL_UART_Transmit(huart,&data,1,10);
}

void STM32SerialPort::write(uint8_t *data,uint8_t length){
    HAL_UART_Transmit(huart,data,length,0xFFFF);
}

bool STM32SerialPort::available(){
	front = (MAXBUFFERSIZE-huart->hdmarx->Instance->NDTR);
	return front!=rear;
}

int STM32SerialPort::read(){
    if(available()){
        uint8_t recv_data = queue[rear];
        rear = (rear+1)%MAXBUFFERSIZE;
        return recv_data;
    }
    else{
        return -1;
    }
}
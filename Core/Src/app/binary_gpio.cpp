#include "binary_gpio.h"

// set gpio pin
void BinaryGPIO::on() {
    HAL_GPIO_WritePin(port, pin, GPIO_PIN_SET);
}

// reset gpio pin
void BinaryGPIO::off() {
    HAL_GPIO_WritePin(port, pin, GPIO_PIN_RESET);
}

// read gpio pin, if pin is initialized input then return current output status
bool BinaryGPIO::read() {
    return HAL_GPIO_ReadPin(port, pin) == GPIO_PIN_SET;
}

void BinaryGPIO::toggle() {
    if (read()) {
        off();
    } else {
        on();
    }
}
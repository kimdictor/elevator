#include "threshold_store.h"

bool thresholdStore::init() {
}

uint8_t* thresholdStore::read(uint8_t doorIndex) {
    uint32_t *ptr = (uint32_t*)STM32F446_THRESHOLD_FLASH_SECTOR + doorIndex * 8;
    return (uint8_t*)ptr;
}

bool thresholdStore::hasRecord(uint8_t doorIndex) {
    uint8_t *data = thresholdStore::read(doorIndex);
    if (data[0] != THRESHOLD_STORE_MARKER) return false;
    if (data[1] != doorIndex) return false;
    return true;
}

bool thresholdStore::getRecord(uint8_t doorIndex, uint16_t *low, uint16_t *high) {
    uint8_t *data = thresholdStore::read(doorIndex);
    if (!hasRecord(doorIndex)) return false;
    *low = data[2] << 8 | data[3];
    *high = data[4] << 8 | data[5];
    return true;
}

bool thresholdStore::setRecord(uint8_t doorIndex, uint16_t low, uint16_t high) {
    if (HAL_FLASH_Unlock() != HAL_OK) return false;
    uint16_t markers = THRESHOLD_STORE_MARKER << 8 | doorIndex;
    uint16_t data[4] = {markers, low, high, 0x00};
    if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, STM32F446_THRESHOLD_FLASH_SECTOR + doorIndex * 8, *((uint64_t *) data)) != HAL_OK) {
        HAL_FLASH_Lock();
        return false;
    }
    if (HAL_FLASH_Lock() != HAL_OK) return false;
    return true;
}
#include "internal_led.h"

void InternalLED::setMode(uint8_t mode) {
    switch(mode) {
        case 0:
            hotLED.off();
            coldLED.off();
            break;
        case 1:
            hotLED.on();
            coldLED.off();
        case 2:
            hotLED.off();
            coldLED.on();
    }
}
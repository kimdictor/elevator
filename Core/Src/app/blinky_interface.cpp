#include "blinky_interface.h"
#include "blinky.h"

extern "C" {
    void blinkyError(uint8_t time) {
        Blinky::blink(&Blinky::Err, time);
    }
    
    void blinkyOn(uint8_t index) {
        Blinky::All[index]->on();
    }
    
    void blinkyOff(uint8_t index) {
        Blinky::All[index]->off();
    }

    void blinkyTest() {
        Blinky::test();
    }

    void blinkyReset() {
        Blinky::reset();
    }
}


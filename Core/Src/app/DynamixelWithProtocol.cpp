#include "DynamixelWithProtocol.h"
#include "DebugPort.h"

DynamixelProtocolController::DynamixelProtocolController()
{
	isModelNumberOkay = false;
	isFirmwareVersionOkay = false;
}

void DynamixelProtocolController::init(
	UART_HandleTypeDef *dxl_serial_port,
  GPIO_TypeDef *dir_port,
  uint16_t dir_pin)
{
	dirPin.init(dir_port,dir_pin);
	port.init(dxl_serial_port);
	packetMaker.infos.ID = DXL_ID;
	id = DXL_ID;
}

/////////////Write///////////// 
//void DynamixelProtocolController::setID(uint8_t _id)
//{
//    packetMaker.infos.address = 7;
//    packetMaker.infos.inst = INSTRUCTIONTYPE_WRITE;
//    packetMaker.infos.paramLength = 1;
//		serialReader.infos.param_buf_capacity=0;
//	
//    packetMaker.infos.param[0] = _id;
//		
//    packetMaker.makePacket();
//    serialWriting(&packetMaker.packet);
//	
//		
//    packetMaker.infos.ID = _id;
//    rxStatusPacket();
//}

bool DynamixelProtocolController::ping()
{
	packetMaker.infos.inst = INSTRUCTIONTYPE_PING;
	packetMaker.infos.ID = DXL_ID;
	packetMaker.makePacket();
	serialWriting(&packetMaker.packet);
	
	serialReader.infos.param_buf_capacity = 3;
	if(rxStatusPacket())
	{
		uint32_t temp = getParam();
		isFirmwareVersionOkay = ((temp&0xFF0000)==VERSION_OF_FIRMWARE);
		isModelNumberOkay = ((temp&0xFFFF) == MODEL_NUMBER);
		if (!isModelNumberOkay) DebugPort::UART_Printf("Dynamixel: Invalid Model Number! Expect %x but Given %x\r\n", MODEL_NUMBER, (temp&0xFFFF));
		if (!isModelNumberOkay) DebugPort::UART_Printf("Dynamixel: Invalid Firmware Version! Expect %x but Given %x\r\n", VERSION_OF_FIRMWARE, (temp&0xFF0000));
		return true;
	}
	else
	{
		return false;
	}
}

void DynamixelProtocolController::setPortBaudRate(uint32_t baudrate)
{
	port.setBaudrate(baudrate);
}

void DynamixelProtocolController::factoryReset()
{
    packetMaker.infos.inst = INSTRUCTIONTYPE_FACTORYRESET;
    packetMaker.makePacket();
    serialWriting(&packetMaker.packet);
	
		serialReader.infos.param_buf_capacity = 0;
		rxStatusPacket();
}

void DynamixelProtocolController::setBaudrate(uint32_t baudrate)
{
	uint8_t baudrateIndex = 0;
	if(baudrate == 57600)
	{
		baudrateIndex = 1;
	}
	else if(baudrate == 115200)
	{
		baudrateIndex = 2;
	}
	else if(baudrate == 1000000)
	{
		baudrateIndex = 3;
	}
	
	packetMaker.infos.address = 8;
	packetMaker.infos.inst = INSTRUCTIONTYPE_WRITE;
	packetMaker.infos.paramLength = 1;
	packetMaker.infos.param[0] = baudrateIndex;
	packetMaker.makePacket();
	serialWriting(&packetMaker.packet);

	serialReader.infos.param_buf_capacity = 0;
	rxStatusPacket();
}

void DynamixelProtocolController::ledOn()
{
    packetMaker.infos.address = 65;
    packetMaker.infos.inst = INSTRUCTIONTYPE_WRITE;
    packetMaker.infos.paramLength = 1;
    packetMaker.infos.param[0] = 1;
    packetMaker.makePacket();
    serialWriting(&packetMaker.packet);
	
		serialReader.infos.param_buf_capacity = 0;
    rxStatusPacket();
}

void DynamixelProtocolController::ledOff()
{
    packetMaker.infos.address = 65;
    packetMaker.infos.inst = INSTRUCTIONTYPE_WRITE;
    packetMaker.infos.paramLength = 1;
    packetMaker.infos.param[0] = 0;
    packetMaker.makePacket();
    serialWriting(&packetMaker.packet);
	
		serialReader.infos.param_buf_capacity = 0;
    rxStatusPacket();
}

void DynamixelProtocolController::torqueOn()
{
    packetMaker.infos.address = 64;
    packetMaker.infos.inst = INSTRUCTIONTYPE_WRITE;
    packetMaker.infos.paramLength = 1;
    packetMaker.infos.param[0] = 1;
    packetMaker.makePacket();
    serialWriting(&packetMaker.packet);
	
	
		serialReader.infos.param_buf_capacity = 0;
    rxStatusPacket();
}

void DynamixelProtocolController::torqueOff()
{
    packetMaker.infos.address = 64;
    packetMaker.infos.inst = INSTRUCTIONTYPE_WRITE;
    packetMaker.infos.paramLength = 1;
    packetMaker.infos.param[0] = 0;
		packetMaker.makePacket();
    serialWriting(&packetMaker.packet);
	
		serialReader.infos.param_buf_capacity = 0;
    rxStatusPacket();
}

void DynamixelProtocolController::operationMode(int mode)
{
    packetMaker.infos.address = 11;
    packetMaker.infos.inst = INSTRUCTIONTYPE_WRITE;
    packetMaker.infos.paramLength = 1;
    packetMaker.infos.param[0] = mode;
    packetMaker.makePacket();
    serialWriting(&packetMaker.packet);
		serialReader.infos.param_buf_capacity = 0;
    rxStatusPacket();
}

void DynamixelProtocolController::reboot()
{
    packetMaker.infos.inst = INSTRUCTIONTYPE_REBOOT;
    packetMaker.makePacket();
    serialWriting(&packetMaker.packet);
		serialReader.infos.param_buf_capacity = 0;
    rxStatusPacket();
}

void DynamixelProtocolController::goalPosition(uint32_t pulse)
{
		// uint32_t pulse = (angle * 4095) / 360;

    packetMaker.infos.address = 116;
    packetMaker.infos.inst = INSTRUCTIONTYPE_WRITE;
    packetMaker.infos.paramLength = 4;

    packetMaker.infos.param[0] = pulse >> 0;
    packetMaker.infos.param[1] = pulse >> 8;
    packetMaker.infos.param[2] = pulse >> 16;
    packetMaker.infos.param[3] = pulse >> 24;

    packetMaker.makePacket();
    serialWriting(&packetMaker.packet);
	
		serialReader.infos.param_buf_capacity = 0;
    rxStatusPacket();
}

//range: 0~2047
//unit: rev/min
//
void DynamixelProtocolController::goalVelocity(int pulse)
{
    packetMaker.infos.address = 104;
    packetMaker.infos.inst = INSTRUCTIONTYPE_WRITE;
    packetMaker.infos.paramLength = 4;

    packetMaker.infos.param[0] = pulse >> 0;
    packetMaker.infos.param[1] = pulse >> 8;
    packetMaker.infos.param[2] = pulse >> 16;
    packetMaker.infos.param[3] = pulse >> 24;

    packetMaker.makePacket();
    serialWriting(&packetMaker.packet);
	
		serialReader.infos.param_buf_capacity = 0;
    rxStatusPacket();
}

/* Set drive mode
https://emanual.robotis.com/docs/kr/dxl/x/xl330-m288/#drive-mode
*/
void DynamixelProtocolController::setDriveMode(bool isReverseMode,bool isTimeBasedProfile,bool isTorqueOnByGoalUpdate)
{
    packetMaker.infos.address = 10;
    packetMaker.infos.inst = INSTRUCTIONTYPE_WRITE;
    packetMaker.infos.paramLength = 1;
		
		packetMaker.infos.param[0] = 0;
		if (isReverseMode){
			packetMaker.infos.param[0] |= 1<<0;
		}
    if (isTimeBasedProfile)
    {
      packetMaker.infos.param[0] |= 1<<2;
    }
		if (isTorqueOnByGoalUpdate){
			packetMaker.infos.param[0] |= 1<<3;
		}
		
    packetMaker.makePacket();
    serialWriting(&packetMaker.packet);
		serialReader.infos.param_buf_capacity = 0;
    rxStatusPacket();
}


void DynamixelProtocolController::setProfileAcceleration(uint32_t acceleration)
{
    packetMaker.infos.address = 108;
    packetMaker.infos.inst = INSTRUCTIONTYPE_WRITE;
    packetMaker.infos.paramLength = 4;
		
    packetMaker.infos.param[0] = acceleration >> 0;
    packetMaker.infos.param[1] = acceleration >> 8;
    packetMaker.infos.param[2] = acceleration >> 16;
    packetMaker.infos.param[3] = acceleration >> 24;

    packetMaker.makePacket();
    serialWriting(&packetMaker.packet);
		serialReader.infos.param_buf_capacity = 0;
    rxStatusPacket();
}

void DynamixelProtocolController::setStatusReturnLevel(uint8_t value){
    packetMaker.infos.address = 68;
    packetMaker.infos.inst = INSTRUCTIONTYPE_WRITE;
    packetMaker.infos.paramLength = 1;
		

    packetMaker.infos.param[0] = value;
		packetMaker.makePacket();
    serialWriting(&packetMaker.packet);
	
		serialReader.infos.param_buf_capacity = 0;
    rxStatusPacket();
}

void DynamixelProtocolController::setProfileVelocity(uint32_t velocity)
{
    packetMaker.infos.address = 112;
    packetMaker.infos.inst = INSTRUCTIONTYPE_WRITE;
    packetMaker.infos.paramLength = 4;

    packetMaker.infos.param[0] = velocity >> 0;
    packetMaker.infos.param[1] = velocity >> 8;
    packetMaker.infos.param[2] = velocity >> 16;
    packetMaker.infos.param[3] = velocity >> 24;

    packetMaker.makePacket();
    serialWriting(&packetMaker.packet);
		serialReader.infos.param_buf_capacity = 0;
		rxStatusPacket();
}

void DynamixelProtocolController::setHomingOffset(uint32_t pulse){
    packetMaker.infos.address = 20;
    packetMaker.infos.inst = INSTRUCTIONTYPE_WRITE;
    packetMaker.infos.paramLength = 4;
	
		//uint32_t homingValue = angle/0.088;
	
    packetMaker.infos.param[0] = pulse >> 0;
    packetMaker.infos.param[1] = pulse >> 8;
    packetMaker.infos.param[2] = pulse >> 16;
    packetMaker.infos.param[3] = pulse >> 24;

    packetMaker.makePacket();
    serialWriting(&packetMaker.packet);
		serialReader.infos.param_buf_capacity = 0;
		rxStatusPacket();	
}

bool DynamixelProtocolController::setMinPositionLimit(uint32_t minPositionLimit){
    packetMaker.infos.address = 52;
    packetMaker.infos.inst = INSTRUCTIONTYPE_WRITE;
    packetMaker.infos.paramLength = 4;
		serialReader.infos.param_buf_capacity = 0;
	
    packetMaker.infos.param[0] = minPositionLimit >> 0;
    packetMaker.infos.param[1] = minPositionLimit >> 8;
    packetMaker.infos.param[2] = minPositionLimit >> 16;
    packetMaker.infos.param[3] = minPositionLimit >> 24;

    packetMaker.makePacket();
    serialWriting(&packetMaker.packet);
		return rxStatusPacket();	
}

bool DynamixelProtocolController::setMaxPositionLimit(uint32_t maxPositionLimit)
{
		packetMaker.infos.address = 52;
    packetMaker.infos.inst = INSTRUCTIONTYPE_WRITE;
    packetMaker.infos.paramLength = 4;
		serialReader.infos.param_buf_capacity = 0;
	
    packetMaker.infos.param[0] = maxPositionLimit >> 0;
    packetMaker.infos.param[1] = maxPositionLimit >> 8;
    packetMaker.infos.param[2] = maxPositionLimit >> 16;
    packetMaker.infos.param[3] = maxPositionLimit >> 24;

    packetMaker.makePacket();
    serialWriting(&packetMaker.packet);
		return rxStatusPacket();
}

void DynamixelProtocolController::setPositionPGain(uint16_t gain)
{
		packetMaker.infos.address = 84;
    packetMaker.infos.inst = INSTRUCTIONTYPE_WRITE;
    packetMaker.infos.paramLength = 2;
		serialReader.infos.param_buf_capacity = 0;
	
    packetMaker.infos.param[0] = gain >> 0;
    packetMaker.infos.param[1] = gain >> 8;

    packetMaker.makePacket();
    serialWriting(&packetMaker.packet);
		rxStatusPacket();
}

void DynamixelProtocolController::setPositionIGain(uint16_t gain)
{
		packetMaker.infos.address = 82;
    packetMaker.infos.inst = INSTRUCTIONTYPE_WRITE;
    packetMaker.infos.paramLength = 2;
		serialReader.infos.param_buf_capacity = 0;
	
    packetMaker.infos.param[0] = gain >> 0;
    packetMaker.infos.param[1] = gain >> 8;

    packetMaker.makePacket();
    serialWriting(&packetMaker.packet);
		rxStatusPacket();
}

void DynamixelProtocolController::setPositionDGain(uint16_t gain)
{
		packetMaker.infos.address = 80;
    packetMaker.infos.inst = INSTRUCTIONTYPE_WRITE;
    packetMaker.infos.paramLength = 2;
		serialReader.infos.param_buf_capacity = 0;
	
    packetMaker.infos.param[0] = gain >> 0;
    packetMaker.infos.param[1] = gain >> 8;

    packetMaker.makePacket();
    serialWriting(&packetMaker.packet);
		rxStatusPacket();
}

/////////////////////READ////////////////////
uint8_t DynamixelProtocolController::getID()
{
    packetMaker.infos.address = 7;
    packetMaker.infos.inst = INSTRUCTIONTYPE_READ;
    packetMaker.infos.paramLength = 2;
		serialReader.infos.param_buf_capacity = 1;
	
    packetMaker.infos.param[0] = serialReader.infos.param_buf_capacity;
		packetMaker.infos.param[1] = 0;
    packetMaker.makePacket();
    serialWriting(&packetMaker.packet);
	
		if(rxStatusPacket())
		{
			return getParam();
		}
		else
		{
			// DebugPort::UART_Printf("getID rxStatusPacket Error\r\n");
			return 0xFF;
		}
}

int DynamixelProtocolController::getPresentPosition()
{
    packetMaker.infos.address = 132;
    packetMaker.infos.inst = INSTRUCTIONTYPE_READ;
    packetMaker.infos.paramLength = 2;
		serialReader.infos.param_buf_capacity = 4;

    packetMaker.infos.param[0] = serialReader.infos.param_buf_capacity;
    packetMaker.infos.param[1] = 0;

    packetMaker.makePacket();
    serialWriting(&packetMaker.packet);
	
		if(rxStatusPacket())
		{
			//position = (position * 360) / 4095;
			return getParam();
		}
		else
		{
			// DebugPort::UART_Printf("getPresentPosition rxStatusPacket Error\r\n");
			return -1;
		}
}

int DynamixelProtocolController::getPresentPWM()
{
    packetMaker.infos.address = 124;
    packetMaker.infos.inst = INSTRUCTIONTYPE_READ;
    packetMaker.infos.paramLength = 2;
		serialReader.infos.param_buf_capacity = 2;

    packetMaker.infos.param[0] = serialReader.infos.param_buf_capacity;
    packetMaker.infos.param[1] = 0;

    packetMaker.makePacket();
    serialWriting(&packetMaker.packet);
		
		if(rxStatusPacket())
		{
			return getParam();
		}
		else
		{
			// DebugPort::UART_Printf("getPresentPWM rxStatusPacket Error\r\n");
			return -1;
		}
}

int DynamixelProtocolController::getPresentCurrent()
{
    packetMaker.infos.address = 126;
    packetMaker.infos.inst = INSTRUCTIONTYPE_READ;
    packetMaker.infos.paramLength = 2;
	serialReader.infos.param_buf_capacity = 2;

    packetMaker.infos.param[0] = serialReader.infos.param_buf_capacity;
    packetMaker.infos.param[1] = 0;

    packetMaker.makePacket();
    serialWriting(&packetMaker.packet);
	
		if(rxStatusPacket())
		{
			//position = (position * 360) / 4095;
			return getParam();
		}
		else
		{
			// DebugPort::UART_Printf("getPresentPosition rxStatusPacket Error\r\n");
			return -1;
		}
}

bool DynamixelProtocolController::getMoving()
{
    packetMaker.infos.address = 122;
    packetMaker.infos.inst = INSTRUCTIONTYPE_READ;
    packetMaker.infos.paramLength = 2;
		serialReader.infos.param_buf_capacity = 1;

    packetMaker.infos.param[0] = serialReader.infos.param_buf_capacity;
    packetMaker.infos.param[1] = 0;

    packetMaker.makePacket();
    serialWriting(&packetMaker.packet);
		
		if(rxStatusPacket())
		{
			return getParam();
		}
		else
		{
			// DebugPort::UART_Printf("getMovingStatus rxStatusPacket Error\r\n");
			return false;
		}
}

bool DynamixelProtocolController::getTorqueEnable()
{
    packetMaker.infos.address = 64;
    packetMaker.infos.inst = INSTRUCTIONTYPE_READ;
    packetMaker.infos.paramLength = 2;
	serialReader.infos.param_buf_capacity = 1;

    packetMaker.infos.param[0] = serialReader.infos.param_buf_capacity;
    packetMaker.infos.param[1] = 0;

    packetMaker.makePacket();
    serialWriting(&packetMaker.packet);
		
		if(rxStatusPacket())
		{
			return getParam();
		}
		else
		{
			// DebugPort::UART_Printf("getTorqueEnable rxStatusPacket Error\r\n");
			return false;
		}
}

uint8_t DynamixelProtocolController::getMovingStatus()
{
    packetMaker.infos.address = 123;
    packetMaker.infos.inst = INSTRUCTIONTYPE_READ;
    packetMaker.infos.paramLength = 2;
		serialReader.infos.param_buf_capacity = 1;

    packetMaker.infos.param[0] = serialReader.infos.param_buf_capacity;
    packetMaker.infos.param[1] = 0;
		
		
    packetMaker.makePacket();
    serialWriting(&packetMaker.packet);
		if(rxStatusPacket())
		{
			return getParam();
		}
		else
		{
			// DebugPort::UART_Printf("getMovingStatus rxStatusPacket Error\r\n");
			return -1;
		}
}

int DynamixelProtocolController::getPositionTrajectory()
{
    packetMaker.infos.address = 140;
    packetMaker.infos.inst = INSTRUCTIONTYPE_READ;
    packetMaker.infos.paramLength = 2;
		serialReader.infos.param_buf_capacity = 4;

    packetMaker.infos.param[0] = serialReader.infos.param_buf_capacity;
    packetMaker.infos.param[1] = 0;
		
    packetMaker.makePacket();
    serialWriting(&packetMaker.packet);
		if(rxStatusPacket())
		{
			return getParam();
		}
		else
		{
			// DebugPort::UART_Printf("getMovingStatus rxStatusPacket Error\r\n");
			return -1;
		}
}

uint8_t DynamixelProtocolController::getHardwareErrorStatus()
{
    packetMaker.infos.address = 70;
    packetMaker.infos.inst = INSTRUCTIONTYPE_READ;
    packetMaker.infos.paramLength = 2;
		serialReader.infos.param_buf_capacity = 1;

    packetMaker.infos.param[0] = serialReader.infos.param_buf_capacity;
    packetMaker.infos.param[1] = 0;
		
    packetMaker.makePacket();
    serialWriting(&packetMaker.packet);
		if(rxStatusPacket())
		{
			return getParam();
		}
		else
		{
			// DebugPort::UART_Printf("getMovingStatus rxStatusPacket Error\r\n");
			return -1;
		}
}


//Decimal from signed 2's complement
//1. invert all bit
//2. plus 1
//3. return minus value
bool DynamixelProtocolController::rxStatusPacket()
{
	currentTime = HAL_GetTick();

	while (true)
	{
		if (port.available())
		{
				uint8_t recv_data = port.read();
			
				if (serialReader.parsePacket(recv_data))
				{
					comsResult = MOTOR_COMS_RESULT_OK;
					port.reset();
					return true;
				} else {
					//DebugPort::UART_Printf("rxStatusPacket: parse fail\r\n");
				}
		} else {
			//DebugPort::UART_Printf("rxStatusPacket: no rx data\r\n");
		}
		if (HAL_GetTick() - currentTime > RX_STATUS_PACKET_TIMEOUT)
		{
			comsResult = MOTOR_COMS_RESULT_READTIMEOUT;
			//DebugPort::UART_Printf("rxStatusPacket: timeout\r\n");
			return false;
		}
	}
}

//Decimal from signed 2's complement
//1. invert all bit
//2. plus 1
//3. return minus value
int DynamixelProtocolController::getParam(){
	int param = 0;
	if(serialReader.infos.param_buf_capacity==0){
		return 0;
	}
	else if(serialReader.infos.param_buf_capacity==1){
		uint8_t param8=serialReader.received_buf[0];
		param = (int8_t)param8;
	}
	else if(serialReader.infos.param_buf_capacity==2){
		uint16_t param16 = 0;
		param16 |= serialReader.received_buf[0];
		param16 |= serialReader.received_buf[1] << 8;
		param = (int16_t)param16;
	}
	else if(serialReader.infos.param_buf_capacity==3 || serialReader.infos.param_buf_capacity==4){
		uint32_t param32 = 0;
		param32 |= serialReader.received_buf[0];
		param32 |= serialReader.received_buf[1] << 8;
		param32 |= serialReader.received_buf[2] << 16;
		param32 |= serialReader.received_buf[3] << 24;
		param = (int32_t)param32;
	}
	return param;
}

MotorComsResult DynamixelProtocolController::getComsResult()
{
    return comsResult;
}

uint16_t DynamixelProtocolController::getPacketAddress()
{
	return packetMaker.infos.address;
}

void DynamixelProtocolController::serialWriting(Packet *packet)
{
    dirPin.setPin();
    port.write(packet->packet_buf, packet->buf_length);
    dirPin.resetPin();
}
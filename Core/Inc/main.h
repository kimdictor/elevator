/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define BUTTON_SWITCH_1_Pin GPIO_PIN_2
#define BUTTON_SWITCH_1_GPIO_Port GPIOE
#define BUTTON_SWITCH_3_Pin GPIO_PIN_4
#define BUTTON_SWITCH_3_GPIO_Port GPIOE
#define DXL_DEBUG_LED_1_Pin GPIO_PIN_0
#define DXL_DEBUG_LED_1_GPIO_Port GPIOF
#define DXL_DEBUG_LED_2_Pin GPIO_PIN_1
#define DXL_DEBUG_LED_2_GPIO_Port GPIOF
#define DXL_DEBUG_LED_3_Pin GPIO_PIN_2
#define DXL_DEBUG_LED_3_GPIO_Port GPIOF
#define DXL_DEBUG_LED_4_Pin GPIO_PIN_3
#define DXL_DEBUG_LED_4_GPIO_Port GPIOF
#define SMART_FILM_1_Pin GPIO_PIN_6
#define SMART_FILM_1_GPIO_Port GPIOF
#define SMART_FILM_2_Pin GPIO_PIN_7
#define SMART_FILM_2_GPIO_Port GPIOF
#define SMART_FILM_3_Pin GPIO_PIN_8
#define SMART_FILM_3_GPIO_Port GPIOF
#define SMART_FILM_4_Pin GPIO_PIN_9
#define SMART_FILM_4_GPIO_Port GPIOF
#define DYNAMIXEL_TX_2_Pin GPIO_PIN_2
#define DYNAMIXEL_TX_2_GPIO_Port GPIOA
#define DYNAMIXEL_RX_2_Pin GPIO_PIN_3
#define DYNAMIXEL_RX_2_GPIO_Port GPIOA
#define DYNAMIXEL_DIR_2_Pin GPIO_PIN_4
#define DYNAMIXEL_DIR_2_GPIO_Port GPIOA
#define DYNAMIXEL_RX_3_Pin GPIO_PIN_5
#define DYNAMIXEL_RX_3_GPIO_Port GPIOC
#define DC_FAN_2_Pin GPIO_PIN_13
#define DC_FAN_2_GPIO_Port GPIOF
#define DC_FAN_3_Pin GPIO_PIN_14
#define DC_FAN_3_GPIO_Port GPIOF
#define DC_FAN_4_Pin GPIO_PIN_15
#define DC_FAN_4_GPIO_Port GPIOF
#define DEBUG_SERIAL_RX_Pin GPIO_PIN_7
#define DEBUG_SERIAL_RX_GPIO_Port GPIOE
#define DEBUG_SERIAL_TX_Pin GPIO_PIN_8
#define DEBUG_SERIAL_TX_GPIO_Port GPIOE
#define EXTERNAL_LED_1_Pin GPIO_PIN_9
#define EXTERNAL_LED_1_GPIO_Port GPIOE
#define EXTERNAL_LED_2_Pin GPIO_PIN_11
#define EXTERNAL_LED_2_GPIO_Port GPIOE
#define DYNAMIXEL_TX_3_Pin GPIO_PIN_8
#define DYNAMIXEL_TX_3_GPIO_Port GPIOD
#define DYNAMIXEL_DIR_3_Pin GPIO_PIN_10
#define DYNAMIXEL_DIR_3_GPIO_Port GPIOD
#define INTERNAL_LED_1_Pin GPIO_PIN_2
#define INTERNAL_LED_1_GPIO_Port GPIOG
#define INTERNAL_LED_2_Pin GPIO_PIN_3
#define INTERNAL_LED_2_GPIO_Port GPIOG
#define INTERNAL_LED_3_Pin GPIO_PIN_4
#define INTERNAL_LED_3_GPIO_Port GPIOG
#define INTERNAL_LED_4_Pin GPIO_PIN_5
#define INTERNAL_LED_4_GPIO_Port GPIOG
#define DYNAMIXEL_TX_4_Pin GPIO_PIN_6
#define DYNAMIXEL_TX_4_GPIO_Port GPIOC
#define DYNAMIXEL_RX_4_Pin GPIO_PIN_7
#define DYNAMIXEL_RX_4_GPIO_Port GPIOC
#define DYNAMIXEL_DIR_4_Pin GPIO_PIN_8
#define DYNAMIXEL_DIR_4_GPIO_Port GPIOC
#define DYNAMIXEL_DIR_1_Pin GPIO_PIN_8
#define DYNAMIXEL_DIR_1_GPIO_Port GPIOA
#define DYNAMIXEL_TX_1_Pin GPIO_PIN_9
#define DYNAMIXEL_TX_1_GPIO_Port GPIOA
#define DYNAMIXEL_RX_1_Pin GPIO_PIN_10
#define DYNAMIXEL_RX_1_GPIO_Port GPIOA
#define PARTITION_SWITCH_1_Pin GPIO_PIN_0
#define PARTITION_SWITCH_1_GPIO_Port GPIOD
#define PARTITION_SWITCH_2_Pin GPIO_PIN_1
#define PARTITION_SWITCH_2_GPIO_Port GPIOD
#define PARTITION_SWITCH_3_Pin GPIO_PIN_2
#define PARTITION_SWITCH_3_GPIO_Port GPIOD
#define LED_RUN_Pin GPIO_PIN_5
#define LED_RUN_GPIO_Port GPIOD
#define LED_ACT_Pin GPIO_PIN_6
#define LED_ACT_GPIO_Port GPIOD
#define LED_ERR_Pin GPIO_PIN_7
#define LED_ERR_GPIO_Port GPIOD
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

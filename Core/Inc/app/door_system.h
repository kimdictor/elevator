#ifndef _DOOR_SYSTEM_H_
#define _DOOR_SYSTEM_H_

#include <stdint.h>
#include <stdlib.h>
#include <usart.h>
#include "cmsis_os.h"
#include "DebugPort.h"
#include "io_pin.h"
#include "DynamixelWithProtocol.h"
#include "signal_config.h"
#include "internal_led.h"
#include "smart_film.h"
#include "dc_fan.h"
//#include "blinky.h"
#include "threshold_store.h"

class Mediator;

// <Motor related>
// 1. dirport, dirpin
// 2. uart port
//
// <Smart Film>
// 1. gpioport,pin
//
// <Internal LED>
// 1. gpioport,pin
//
// <DC_FAN>

#define LOOP_INTERVAL 5
#define OPEN_THRESHOLD 100
#define CLOSE_THRESHOLD 100
#define TOTAL_TIME 2800
#define ACCEL_TIME 1200
#define DELAY_GOALPOSITION 50

#define OPENED_PULSE_LEFT 73
#define CLOSED_PULSE_LEFT 1871
#define OPENED_PULSE_RIGHT 1658 
#define CLOSED_PULSE_RIGHT 3853

enum DoorCommandArgument 
{
	CLOSE = 0x00,
	OPEN = 0x01,
	ON = 0x01,
	OFF = 0x00
};

enum DoorPolarity {
	DOORPOLARITY_LEFT,
	DOORPOLARITY_RIGHT
};

enum DoorSystemError
{
	DOORSYSTEMERROR_NONE,
	DOORSYSTEMERROR_THREAD_INIT_FAILED,
	DOORSYSTEMERROR_MOTOR_PING_FAILED,
	DOORSYSTEMERROR_MOTOR_SET_BAUD_FAILED,
	DOORSYSTEMERROR_MOTOR_COMM_FAILED,
	DOORSYSTEMERROR_MOTOR_DISCONNECTED,
	DOORSYSTEMERROR_MOTOR_MODEL_ERROR,
	DOORSYSTEMERROR_MOTOR_HARDWARE_ERROR
};

enum DoorState{
	DOORSTATE_INIT,
	DOORSTATE_OPENED,
	DOORSTATE_CLOSED,
	DOORSTATE_OPENING,
	DOORSTATE_CLOSING,
	DOORSTATE_RECOVERYOPENING,
	DOORSTATE_RECOVERYCLOSING,
	DOORSTATE_REBOOT
};

class DoorSystem
{
private:
	osThreadId threadID;
	Mediator *mediator;

	DynamixelProtocolController motor;
	InternalLED* led;
	DCFan* fan;
	SmartFlim* flim;

	uint32_t commandTime;

	uint8_t pwmErrorCount;
	uint16_t pwmPassContinuousCount;
	uint32_t loopTick;
	uint16_t averageLoopInterval;

	DoorPolarity polarity;
	
	uint16_t lowPulse, highPulse;
	uint8_t errorLedIndex;
	

	volatile uint8_t command_arg;

	void openDoor();
	void closeDoor();
	void stop();
	void run();
	bool initEEPROM();
	bool initRAM();
	void initDoor();
	bool setBaudrate();
	void repeatTest();
	void commandToMotor(void (DynamixelProtocolController::*f)(int),int param);
	void error(DoorSystemError err);
	void log(char* format, ...);
	void ThresholdAdjust();

public:
	DoorSystem();
	DoorState state;
	
	bool init(UART_HandleTypeDef *dxl_serial_port, GPIO_TypeDef *dir_port, uint16_t dir_pin, DoorPolarity polarity, uint8_t errorLedIndex);	
	void assignLed(InternalLED* led);
	void assignLed(GPIO_TypeDef* _hotport, uint16_t _hotpin, GPIO_TypeDef* _coldport, uint16_t _coldpin);
	void assignFan(DCFan* fan);
	void assignFan(GPIO_TypeDef* _port, uint16_t _pin);
	void assignFlim(SmartFlim* flim);
	void assignFlim(GPIO_TypeDef* _port, uint16_t _pin);

	uint8_t getStatusFlag();

	void task();
	osThreadId getThreadID();
	void setMediator(Mediator *_mediator);
	void setCommandArg(uint8_t _command_arg);

	static const char* errorToString(DoorSystemError err); 
	static const char* stateToString(DoorState state); 
};

#endif // DOOR_SYSTEM
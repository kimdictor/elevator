#ifndef INC_BINARY_GPIO_H_
#define INC_BINARY_GPIO_H_

#include "gpio.h"

// BinaryGPIO is simple wrapper for digital GPIO. This provide turn on and off the given GPIO. 
// If device class like dc fan or internal led just do only on/off gpio then, can inherit this for more simple implementation.
class BinaryGPIO {
    private:
        GPIO_TypeDef* port;
        uint16_t pin;
    public:
        BinaryGPIO() {}
        // this initializer is equal to init() method.
        BinaryGPIO(GPIO_TypeDef* _port, uint16_t _pin):port(_port), pin(_pin) {}
        void init(GPIO_TypeDef* _port, uint16_t _pin) {
            port = _port;
            pin = _pin;
        }
        void on();
        void off();
        void toggle();
        bool read();
};

class InverseBinaryGPIO : public BinaryGPIO {
    public:
        InverseBinaryGPIO(): BinaryGPIO() {}
        InverseBinaryGPIO(GPIO_TypeDef* _port, uint16_t _pin): BinaryGPIO(_port, _pin) {}
        void on() { BinaryGPIO::off(); }
        void off() { BinaryGPIO::on(); }
        bool read() { return !BinaryGPIO::read(); }
};

#endif
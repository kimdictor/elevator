#ifndef SerialReader_H
#define SerialReader_H

#include "crcCalculator.h"

typedef struct ParsingInfos
{
    uint8_t id;
    uint8_t inst;
		uint16_t param_buf_capacity;
    uint16_t length;
    uint32_t param;
    uint16_t crc;
    uint16_t recv_crc;
} ParsingInfos;


enum ParsingState
{
	HEADERSTATE,
	RESERVEDSTATE,
	IDSTATE,
	LLSTATE,
	LHSTATE,
	INSTSTATE,
	ERRORSTATE,
	PARAMSTATE,
	CRCLSTATE,
	CRCHSTATE
};

class DynamixelSerialReader
{
private:
	ParsingState state;
	
	uint8_t header_count;
	uint8_t header[3];
	uint16_t received_length;

public:
	uint8_t received_buf[4];
	ParsingInfos infos;
	bool parsePacket(uint8_t recv_data);
	ParsingState getState();
};
#endif

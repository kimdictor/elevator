#ifndef INC_INTERNAL_LED_H_
#define INC_INTERNAL_LED_H_

#include "binary_gpio.h"



class InternalLED {
    private:
        BinaryGPIO hotLED;
        BinaryGPIO coldLED;
    public:
        InternalLED() {};
        void init(GPIO_TypeDef* _hotport, uint16_t _hotpin, GPIO_TypeDef* _coldport, uint16_t _coldpin) {
            hotLED.init(_hotport, _hotpin);
            coldLED.init(_coldport, _coldpin);
        }
        void setMode(uint8_t mode);
};

#endif
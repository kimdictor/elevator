#ifndef _DEVICE_CONFIG_H_
#define _DEVICE_CONFIG_H_

#include "usart.h"
#include "can.h"

#define DEBUG_SERIAL huart5
#define CAN_PORT hcan1

#define DXL_SERIAL_1 huart1
#define DXL_SERIAL_2 huart2
#define DXL_SERIAL_3 huart3
#define DXL_SERIAL_4 huart6

#endif //_MAIN_COMS_H_
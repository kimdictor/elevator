#ifndef _APP_MAIN_H_
#define _APP_MAIN_H_

#include "cmsis_os.h"
#include "blinky_interface.h"

#ifdef __cplusplus
extern "C" {
#endif

void app_main(void);

#ifdef __cplusplus
}
#endif

#endif // _APP_MAIN_H_
#ifndef _MAIN_COMS_H_
#define _MAIN_COMS_H_

#include "cmsis_os.h"
#include "device_config.h"
#include "signal_config.h"
#include <stdint.h>
#include <string.h>
#include "blinky.h"

#define CAN_RELAY_MAX_DATA_LEN (8)

class Mediator;

typedef struct _CanRxMessage {
  CAN_RxHeaderTypeDef header;
  uint8_t data[CAN_RELAY_MAX_DATA_LEN];
} CanRxMessage;

typedef struct _CanTxMessage {
  CAN_TxHeaderTypeDef header;
  uint8_t data[CAN_RELAY_MAX_DATA_LEN];
  uint32_t mailbox;
} CanTxMessage;

class MainComsController {
private:
  osThreadId threadID;
  osMailQId rx_queue; //!< Queue for received messages
  osMailQId tx_queue; //!< Queue for transmit messages
  Mediator *mediator;
  CAN_HandleTypeDef *can_port;

  CanTxMessage *tx_msgs[3]; //!< Storage for in-transit messages

  bool init_can();

public:
  bool init();
  void task();
  void setMediator(Mediator *_mediator);
  CAN_HandleTypeDef *get_can_port();

  bool Transmit(CAN_TxHeaderTypeDef *_header, uint8_t *_data);
  bool Receive(uint32_t fifo);
  bool ProcessRxMessages(void);
  bool ProcessTxMessages(void);
  bool HandleError(void);
  bool CompleteTx(uint32_t mailbox);
  bool AbortTx(uint32_t mailbox);
  osThreadId GetThreadId(void) { return threadID; };
};

#ifdef __cplusplus
extern "C" {
#endif

extern MainComsController main_coms;
#ifdef __cplusplus
}
#endif

#endif // _MAIN_COMS_H_

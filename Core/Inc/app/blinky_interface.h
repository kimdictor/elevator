#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define BLINKY_INDEX_RUN 4

void blinkyError(uint8_t time);
void blinkyOn(uint8_t index);
void blinkyOff(uint8_t index);
void blinkyTest();
void blinkyReset();

#ifdef __cplusplus
}
#endif
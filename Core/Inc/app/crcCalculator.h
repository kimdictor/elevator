#ifndef CRCCALCULATOR_H
#define CRCCALCULATOR_H

#include <stdint.h>

class CRCCalculator
{
private:
    static const unsigned short crc_table[256];
public:
    static void update_crc(uint16_t *p_crc_cur, uint8_t recv_data);
};
#endif

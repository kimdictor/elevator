#ifndef DEBUGPORT_H
#define DEBUGPORT_H
#include "usart.h"
#include "stdarg.h"
#include "stdio.h"

class DebugPort
{
private:
    static UART_HandleTypeDef * huart;
    static uint8_t *print_buffer;
public:
    static void UART_Printf(char *format, ...);
    static void setPort(UART_HandleTypeDef * _huart){
        huart = _huart;
    }
};
#endif
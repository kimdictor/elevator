#ifndef INC_PARTITION_SENSOR_H_
#define INC_PARTITION_SENSOR_H_

#include "binary_gpio.h"

class PartitionSensor : public BinaryGPIO {
    public:
        PartitionSensor(): BinaryGPIO() {}
};

#endif
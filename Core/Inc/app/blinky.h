#ifndef INC_BLINKY_H
#define INC_BLINKY_H

#include "binary_gpio.h"
#include "main.h"

#define BLINKY_DYNAMIXEL_COUNT 4
#define BLINKY_COUNT BLINKY_DYNAMIXEL_COUNT + 3

class Blinky
{
    public:
        static InverseBinaryGPIO Dynamixel1Error;
        static InverseBinaryGPIO Dynamixel2Error;
        static InverseBinaryGPIO Dynamixel3Error;
        static InverseBinaryGPIO Dynamixel4Error;
        static InverseBinaryGPIO Run;
        static InverseBinaryGPIO Act;
        static InverseBinaryGPIO Err;
        static constexpr InverseBinaryGPIO* DynamixelError[4] = {&Blinky::Dynamixel1Error, &Blinky::Dynamixel2Error, &Blinky::Dynamixel3Error, &Blinky::Dynamixel4Error};
        static constexpr InverseBinaryGPIO* All[7] = {&Blinky::Dynamixel1Error, &Blinky::Dynamixel2Error, &Blinky::Dynamixel3Error, &Blinky::Dynamixel4Error, &Blinky::Run, &Blinky::Act, &Blinky::Err};
				
        static void blink(InverseBinaryGPIO* io, uint8_t time);
        static void test();
        static void reset();
};
#endif
#ifndef PACKET_H
#define PACKET_H

#include "crcCalculator.h"

enum InstructionType
{
		INSTRUCTIONTYPE_PING=0x01,
    INSTRUCTIONTYPE_READ=0x02,
    INSTRUCTIONTYPE_WRITE=0x03,
    INSTRUCTIONTYPE_FACTORYRESET=0x06,
    INSTRUCTIONTYPE_REBOOT=0x08
};

struct PacketInfos
{
    uint8_t ID;
    uint8_t param[4];
    uint8_t paramLength;
    uint16_t address;
    InstructionType inst;
};

struct Packet
{
    uint8_t packet_buf[16];
    uint8_t buf_length;
};

class DynamixelPacketMaker
{
public:
    Packet packet;
    PacketInfos infos;

    DynamixelPacketMaker();

    void makePacket();
};
#endif
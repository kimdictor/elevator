#ifndef _MEDIATOR_H_
#define _MEDIATOR_H_

#include <stdint.h>
#include <usart.h>
#include "main_coms.h"
#include "door_system.h"
#include "DebugPort.h"
#include "external_led.h"
#include "internal_led.h"
#include "dc_fan.h"
#include "smart_film.h"
#include "gpio.h"
#include "partition_sensor.h"
#include "blinky.h"
#include "threshold_store.h"

/* hardware config area */
// ELEVATOR_DOOR_SYSTEM_COUNT is door count which want to enable. if 2, bottom two doors is disabled. Default is 4.
#define ELEVATOR_DOOR_SYSTEM_COUNT 2 

class DoorSystemHardware {
	public:
		UART_HandleTypeDef *MotorUART;
		GPIO_TypeDef *MotorDirPort;
		uint16_t MotorDirPin;
		DoorPolarity MotorPolarity;
		GPIO_TypeDef *InternalLedPort;
		uint16_t InternalLedPin;
		GPIO_TypeDef *SmartFlimPort;
		uint16_t SmartFlimPin;
		GPIO_TypeDef *DCFanPort;
		uint16_t DCFanPin;
		uint8_t index;
		
		DoorSystemHardware(UART_HandleTypeDef *_MotorUART,
		GPIO_TypeDef *_MotorDirPort,
		uint16_t _MotorDirPin,
		DoorPolarity _MotorPolarity,
		GPIO_TypeDef *_InternalLedPort,
		uint16_t _InternalLedPin,
		GPIO_TypeDef *_SmartFlimPort,
		uint16_t _SmartFlimPin,
		GPIO_TypeDef *_DCFanPort,
		uint16_t _DCFanPin, uint8_t _index):MotorUART(_MotorUART), MotorDirPort(_MotorDirPort), MotorDirPin(_MotorDirPin), InternalLedPort(_InternalLedPort), InternalLedPin(_InternalLedPin),SmartFlimPort(_SmartFlimPort), SmartFlimPin(_SmartFlimPin), DCFanPort(_DCFanPort), DCFanPin(_DCFanPin), index(_index) {};
};

const DoorSystemHardware DoorSystemHardwareDef[4] = {
	DoorSystemHardware(&DXL_SERIAL_1, DYNAMIXEL_DIR_1_GPIO_Port, DYNAMIXEL_DIR_1_Pin, DOORPOLARITY_LEFT, INTERNAL_LED_1_GPIO_Port, INTERNAL_LED_1_Pin, SMART_FILM_1_GPIO_Port, SMART_FILM_1_Pin, GPIOF, GPIO_PIN_12, 0),
	DoorSystemHardware(&DXL_SERIAL_2, DYNAMIXEL_DIR_2_GPIO_Port, DYNAMIXEL_DIR_2_Pin, DOORPOLARITY_RIGHT, INTERNAL_LED_2_GPIO_Port, INTERNAL_LED_2_Pin, SMART_FILM_2_GPIO_Port, SMART_FILM_2_Pin, DC_FAN_2_GPIO_Port, DC_FAN_2_Pin, 1),
	DoorSystemHardware(&DXL_SERIAL_3, DYNAMIXEL_DIR_3_GPIO_Port, DYNAMIXEL_DIR_3_Pin, DOORPOLARITY_LEFT, INTERNAL_LED_3_GPIO_Port, INTERNAL_LED_3_Pin, SMART_FILM_3_GPIO_Port, SMART_FILM_3_Pin, DC_FAN_3_GPIO_Port, DC_FAN_3_Pin, 2),
	DoorSystemHardware(&DXL_SERIAL_4, DYNAMIXEL_DIR_3_GPIO_Port, DYNAMIXEL_DIR_3_Pin, DOORPOLARITY_RIGHT, INTERNAL_LED_4_GPIO_Port, INTERNAL_LED_4_Pin, SMART_FILM_4_GPIO_Port, SMART_FILM_4_Pin, DC_FAN_4_GPIO_Port, DC_FAN_4_Pin, 3)
};
/* END OF hardware config area*/

#define ELEVATOR_CMD_CONTROL_DOOR      0x01
#define ELEVATOR_CMD_SET_INTERNAL_LED  0x02
#define ELEVATOR_CMD_SET_SMART_FILM    0x03
#define ELEVATOR_CMD_SET_EXTERNAL_LED  0x11
#define ELEVATOR_CMD_CONTROL_DC_FAN 	0x04
#define ELEVATOR_CMD_THRESHOLD_ADJUST 0x05

class Mediator
{
private:
	DoorSystem door_systems[ELEVATOR_DOOR_SYSTEM_COUNT];
	PartitionSensor partition_sensors[3];
	ExternalLED external_leds[2];
	InternalLED internal_leds[ELEVATOR_DOOR_SYSTEM_COUNT];
	DCFan dc_fans[ELEVATOR_DOOR_SYSTEM_COUNT];
	SmartFlim smart_films[ELEVATOR_DOOR_SYSTEM_COUNT];

public:
	void init();
	void processCommand(uint8_t door_index, uint8_t inst, uint8_t arg);
	uint8_t getStatusFlag();
	void statusTask();
};

#endif // _MEDIATOR_H_
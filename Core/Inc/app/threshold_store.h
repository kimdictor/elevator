#ifndef _THRESHOLD_STORE_H_
#define _THRESHOLD_STORE_H_

#include <stdint.h>
#include "stm32f4xx_hal.h"

/*
after STM32F446_THRESHOLD_FLASH_SECTOR
+0 : MARKER
+1 : INDEX
+2 : HIGH H
+3 : HIGH L
+4 : LOW H
+5 : LOW L
+6 : RESERVED
+7 : RESERVED
+8 : MARKER
+9 : INDEX
continue..... 
*/
#define STM32F446_THRESHOLD_FLASH_SECTOR 0x08060000
#define THRESHOLD_STORE_MARKER 0xCE

class thresholdStore {
    private:
        static uint8_t* read(uint8_t doorIndex);
    public:
        bool static init(); 
        bool static hasRecord(uint8_t doorIndex);
        bool static getRecord(uint8_t doorIndex, uint16_t *low, uint16_t *high);
        bool static setRecord(uint8_t doorIndex, uint16_t low, uint16_t high);
};

#endif
/** ***************************************************************************
//
//  @file io_pin.h
//
//  Project:  Ologic STM Library.
//
//  Created: Ralph Gnauck  - Ologicinc.com
//
//  @brief Class to read/set/reset/togle IO Pins
//
//  Copyright (C) ologicinc.com, all rights reserved, 2015,2016.
//
// *****************************************************************************
*/

#ifndef _IO_PIN_H_
#define _IO_PIN_H_

#include "gpio.h"

#ifdef __cplusplus

/**
 *  @class IOPin
 *
 *  @brief Class to handle bit banging GPIO pins
 */
class IOPin {

private:
  GPIO_TypeDef *GPIOx; ///< GPIO Port
  uint16_t GPIO_Pin;   ///< Pin Number

public:
	inline void init(GPIO_TypeDef *_GPIOx, uint16_t _GPIO_Pin)
	{
		GPIOx = _GPIOx;
		GPIO_Pin = _GPIO_Pin;
	}
  /// Set IO Pin High
  inline void setPin(void) {
    HAL_GPIO_WritePin(GPIOx, GPIO_Pin, GPIO_PIN_SET);
  };

  /// Set IO Pin to value in state param
  inline void setPinTo(GPIO_PinState state) {
    HAL_GPIO_WritePin(GPIOx, GPIO_Pin, state);
  };

  /// Set IO Pin Low
  inline void resetPin(void) {
    HAL_GPIO_WritePin(GPIOx, GPIO_Pin, GPIO_PIN_RESET);
  };

  /// Toggle current state of IO Pin
  inline void togglePin(void) { HAL_GPIO_TogglePin(GPIOx, GPIO_Pin); };

  /// Returns current state of IO Pin
  inline GPIO_PinState readPin(void) {
    return HAL_GPIO_ReadPin(GPIOx, GPIO_Pin);
  };
};

#endif // __cplusplus

#endif // _IO_PIN_H_

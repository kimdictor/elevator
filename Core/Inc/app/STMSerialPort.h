#ifndef STM32SERIALPORT_H
#define STM32SERIALPORT_H

#define MAXBUFFERSIZE 64

#include "usart.h"

class STM32SerialPort
{
private:
    UART_HandleTypeDef *huart;
    uint8_t queue[MAXBUFFERSIZE];
    uint32_t front;
    uint32_t rear;

public:
    STM32SerialPort();
		virtual void init(UART_HandleTypeDef *_huart);
    virtual void write(uint8_t data);
    virtual void write(uint8_t *data, uint8_t length);
    virtual bool available();
    virtual int read();
		virtual void reset();
		virtual void setBaudrate(uint32_t baudrate);
    virtual void checkError();
};
#endif

#ifndef _SIGNAL_CONFIG_H_
#define _SIGNAL_CONFIG_H_

class GlobalSignals
{
	public:
		enum{
			kNone = 0,
			kSuspend = (1<<0),
			kResume = (1<<1),
			kLast = (1<<2)
		};
};

class MainComsSignals: public GlobalSignals
{
	public:
		enum 
		{
			kReceive = kLast,
			kTransmit = (kLast << 1),
			kError = (kLast << 2)
		};
};

class DoorSystemSignals: public GlobalSignals
{
	public:
		enum
		{
			DOOR_SYSTEM_SIGNAL_CONTROL_DOOR = kLast,
			DOOR_SYSTEM_SIGNAL_SET_INTERNAL_LED = (kLast<<1),
			DOOR_SYSTEM_SIGNAL_SET_SMART_FILM = (kLast<<2),
			DOOR_SYSTEM_SIGNAL_SET_DC_FAN = (kLast<<3),
			DOOR_SYSTEM_SIGNAL_THRESHOLD_ADJUST = (kLast<<4)
		};
};

#endif //_SIGNAL_CONFIG_H_
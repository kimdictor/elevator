#ifndef DYNAMIXELCONTROLPROTOCOL_H
#define DYNAMIXELCONTROLPROTOCOL_H

#define DEBUG_MODE

#include "stdint.h"
#include "Packet.h"
#include "STMSerialPort.h"
#include "DebugPort.h"
#include "SerialReader.h"
#include "STMSerialPort.h"
#include "io_pin.h"
#include "cmsis_os.h"

#define DXL_ID 1

#define MODEL_NUMBER 0x0438
#define VERSION_OF_FIRMWARE 0x2D0000

#define RX_STATUS_PACKET_TIMEOUT 20

enum MotorComsResult
{
    MOTOR_COMS_RESULT_OK,
    MOTOR_COMS_RESULT_READTIMEOUT
};

class DynamixelProtocolController
{
public:
    DynamixelProtocolController();
    MotorComsResult comsResult;

		bool isModelNumberOkay;
		bool isFirmwareVersionOkay;


		virtual void init(UART_HandleTypeDef *dxl_serial_port,GPIO_TypeDef *dir_port,uint16_t dir_pin);
    // virtual void setID(uint8_t _id);
		virtual bool ping();
		virtual void setBaudrate(uint32_t baudrate);
		virtual void factoryReset();
    virtual uint8_t getID();
    virtual void torqueOn();
    virtual void torqueOff();
    virtual void ledOn();
    virtual void ledOff();
    virtual void goalPosition(uint32_t pulse);
		virtual void goalVelocity(int pulse);
    virtual void reboot();
    virtual int getPresentPosition();
    virtual int getPresentPWM();
    virtual int getPresentCurrent();
    virtual bool getMoving();
    virtual bool getTorqueEnable();
    virtual uint8_t getMovingStatus();
    virtual void setDriveMode(bool isReverseMode,bool isTimeBasedProfile,bool isTorqueOnByGoalUpdate);
    virtual void setProfileAcceleration(uint32_t acceleration);
    virtual void operationMode(int mode);
    virtual void setProfileVelocity(uint32_t velocity);
    virtual void setStatusReturnLevel(uint8_t value);
		virtual void setHomingOffset(uint32_t pulse);
		virtual int getPositionTrajectory();
		virtual void setPortBaudRate(uint32_t baudrate);
		virtual void setPositionPGain(uint16_t gain);
		virtual void setPositionIGain(uint16_t gain);
		virtual void setPositionDGain(uint16_t gain);
		virtual uint8_t getHardwareErrorStatus();
		
    virtual MotorComsResult getComsResult();
		virtual uint16_t getPacketAddress();
		virtual bool setMinPositionLimit(uint32_t minPositionLimit);
		virtual bool setMaxPositionLimit(uint32_t maxPositionLimit);

private:
    uint8_t id;

    STM32SerialPort port;
    IOPin dirPin;

    DynamixelPacketMaker packetMaker;
    DynamixelSerialReader serialReader;

		unsigned long currentTime;
    unsigned long timeout;
    
		
    bool rxStatusPacket();
    void serialWriting(Packet *packet);
    int getParam();
};
#endif

/*
 * neopixel.h
 *
 *  Created on: Sep 13, 2021
 *      Author: chandler
 *  Modified on: Feb 21, 2022
 *      Modify : Jeonghyun
 */

#ifndef INC_EXTERNAL_LED_H_
#define INC_EXTERNAL_LED_H_

/*
	NeoPixel (WS2812) requires below conditions
	- Data Pulse freq : typ 800KHz (In F446, We can meet this on TIM1 through Prescaler = 4 and Fcpu = 180MHz, APB2 Timer Clock = 180MHz)
	- In high bit, High pulse must be holded above 60% of entire pulse.
	- In Low bit, High pulse must be holded below 40% of entire pulse.
	- Packet of one pixel is consisted of 24bit pulse. 8bits of G and R, B.
	- On every end of packet, data line must be kept Low above 50us. In given timer condition of above, this is almost 70 pulses. 
*/
#define NEOPIXEL_LED 32
#define NEOPIXEL_COLOR_PER_LED 3
#define NEOPIXEL_PACKET_SIZE_PER_LED 24
#define NEOPIXEL_PACKET_SIZE_PER_COLOR NEOPIXEL_PACKET_SIZE_PER_LED / NEOPIXEL_COLOR_PER_LED
#define NEOPIXEL_RESET_CODE_SIZE 70
#define NEOPIXEL_DATA_LENGTH NEOPIXEL_LED * NEOPIXEL_COLOR_PER_LED
#define NEOPIXEL_DMADATA_LENGTH NEOPIXEL_LED * NEOPIXEL_PACKET_SIZE_PER_LED + NEOPIXEL_RESET_CODE_SIZE
#define NEOPIXEL_HIGH_PULSE_COUNT 40
#define NEOPIXEL_LOW_PULSE_COUNT 20

// class constant macro definition
#define EXTERNAL_LED_MODE_COUNT 4

#include <stdint.h>
#include <string.h>

#include "DebugPort.h"
#include "tim.h"

class ExternalLED
{
private:
	TIM_HandleTypeDef *timHandle;
	uint8_t timChannel;
	uint8_t data[NEOPIXEL_DATA_LENGTH];
	uint32_t dmaBuffer[NEOPIXEL_DMADATA_LENGTH]; // In formaly, this can be defined as uint8_t. However there is unknown error during mem byte to periph word dma process. So, keep this word temporarily.
	const constexpr static uint8_t modeRGB[EXTERNAL_LED_MODE_COUNT][3] = {
		{255, 255, 255},
		{0, 0, 0},
		{0, 255, 255},
		{255, 255, 0}
	};
	uint8_t mode;

public:
	ExternalLED()
	{
	}
	void init(TIM_HandleTypeDef * handle, uint8_t channel);
	void setLED(int number, uint8_t R, uint8_t G, uint8_t B);
	void setLEDAll(uint8_t R, uint8_t G, uint8_t B);
	void setMode(uint8_t mode);
	uint8_t getMode();
	void apply();
	void stop();
};


#endif /* INC_NEOPIXEL_H_ */
